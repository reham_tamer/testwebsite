<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.login');
});
Auth::routes();

//Route::group(['prefix'=>LaravelLocalization::setLocale()],function(){

    Route::group(['namespace'=>'Website','as'=>'website.'],function (){
        Route::get('/', 'IndexController@index');
        Route::resource('/courses', 'CoursesController');

        Route::get('/register_courses', 'CoursesController@registerAll');
        Route::get('/ajax_category/{id}', 'CoursesController@ajaxCategory');
        Route::get('/ajax_course/{id}', 'CoursesController@ajaxCourse');
        Route::get('/ajax_course_datails/{id}', 'CoursesController@ajaxCourseDetails');
        Route::get('/course_single/{id}', 'CoursesController@courseSingle');
        Route::get('/course_details/{id}', 'CoursesController@coursedetails');
        Route::get('/register_single_course/{id}', 'CoursesController@registerSingle');
        Route::get('/all_in_category/{id}', 'CoursesController@allinCategory');
        Route::get('/all_courses/', 'CoursesController@allCourses');
        Route::get('/available_courses/', 'CoursesController@availableCourses');
        Route::get('/about_us/', 'CoursesController@aboutUs');
        Route::get('/privacy/', 'CoursesController@privacy');
        Route::get('/cancellation/', 'CoursesController@cancellation');
        Route::get('/terms/', 'CoursesController@terms');
        Route::get('/services/{id}', 'CoursesController@showService');
        Route::get('/our_partner/', 'CoursesController@ourPartner');
        Route::get('/methodology/', 'CoursesController@Methodology');
        Route::get('/all_clients/', 'CoursesController@allClients');
        Route::get('/corporate/', 'CoursesController@corporate');
        Route::get('/company/', 'CoursesController@publicc');
        Route::get('/elearning/', 'CoursesController@elearning');
        Route::get('/weekend/', 'CoursesController@weekend');
        Route::resource('/contact', 'ContactUsController');
        Route::resource('/callback', 'CallBackController');
        Route::get('/categories', 'IndexController@categories');
        Route::get('/showmainservices/', 'CoursesController@showMainService');
        Route::get('/subservices/{id}', 'CoursesController@showSubService');

        Route::get('/pay', 'CartController@pay');
        Route::get('/shopping-cart', 'CartController@shoppingCart');
        Route::get('/invoice-details', 'CartController@invoice');
        Route::resource('/carts', 'CartController');
        Route::get('add/cart/{id}','CartController@addItem');
        Route::get('minus/cart/{id}','CartController@minusItem');
        Route::get('delete/cart/{id}','CartController@deleteItem')->name('delete.cart.update');

        Route::get('/confirmation', 'CartController@confirmation');

        Route::get('/training_methodology', 'CoursesController@trainingMethodology');
        Route::get('/blogs', 'CoursesController@showBlogs');
        Route::get('/blog_single/c', 'CoursesController@showSingleBlog');
        Route::get('/search_blogs', 'CoursesController@searchBlogs');

        Route::patch('/settings', 'SettingController@update')->name('settings.update');

    });

//});

//admin
Route::group(array('prefix' => 'dashboard','as'=>'admin.', 'middleware' => 'admin'), function() {
    Route::get('/', 'Admin\IndexController@index');

    //GEO

    Route::resource('course', 'Admin\CourseController');
    Route::resource('category', 'Admin\CategoryController');
    Route::resource('slider', 'Admin\SliderController');
    Route::resource('city', 'Admin\CityController');
    Route::resource('date', 'Admin\DateController');
    Route::resource('requestcourse', 'Admin\RequestCourseController');
    Route::resource('callmeback', 'Admin\CallMeBackController');
    Route::resource('contactus', 'Admin\ContactUsController');
    Route::resource('slider', 'Admin\SliderController');
    Route::resource('user', 'Admin\UserController');
    Route::resource('clients', 'Admin\ClientController');
    Route::resource('partners', 'Admin\PartnerController');
    Route::resource('services', 'Admin\ServicesController');
    Route::resource('blogs', 'Admin\BlogController');
    Route::resource('instructors', 'Admin\InstructorController');
    Route::resource('mainservices', 'Admin\MainServiceController');
    Route::resource('credits', 'Admin\CreditController');

    Route::resource('coursecities', 'Admin\CourseCitiesController');
    Route::get('coursecities/{id}/all', 'Admin\CourseCitiesController@all');
    Route::get('coursecities/create/{id}', 'Admin\CourseCitiesController@createcity');

    Route::get('course/{id}/approve', 'Admin\CourseController@approve');
    Route::get('course/{id}/un-approve', 'Admin\CourseController@unApprove');

});