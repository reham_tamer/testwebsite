
var minusClicks = 0;
var staticQty;
$('.add').click(function(){
    var el = $(this).parent().children('.number');
    var value = parseInt(el.val(), 10);
    value = isNaN(value) ? 0 : value;
    value++;
    el.val(value);
});

$('.minus').click(function(){
    var el = $(this).parent().children('.number');
    var value = parseInt(el.val(), 10);
    value = isNaN(value) ? 0 : value;
    value < 1 ? value = 1 : '';
    value--;
    el.val(value);
});
/*** Start Quantity ***/
//function increaseValue() {
//    var value = parseInt(document.getElementById('number').value, 10);
//    value = isNaN(value) ? 0 : value;
//    value++;
//    document.getElementById('number').value = value;
//}
//
//function decreaseValue() {
//    var value = parseInt(document.getElementById('number').value, 10);
//    value = isNaN(value) ? 0 : value;
//    value < 1 ? value = 1 : '';
//    value--;
//    document.getElementById('number').value = value;
//}
//////////////

//function increaseValue2() {
//    var value = parseInt(document.getElementById('number2').value, 10);
//    value = isNaN(value) ? 0 : value;
//    value++;
//    document.getElementById('number2').value = value;
//}
//
//function decreaseValue2() {
//    var value = parseInt(document.getElementById('number2').value, 10);
//    value = isNaN(value) ? 0 : value;
//    value < 1 ? value = 1 : '';
//    value--;
//    document.getElementById('number2').value = value;
//}
//////////////////
//
//function increaseValue3() {
//    var value = parseInt(document.getElementById('number3').value, 10);
//    value = isNaN(value) ? 0 : value;
//    value++;
//    document.getElementById('number3').value = value;
//}
//
//function decreaseValue3() {
//    var value = parseInt(document.getElementById('number3').value, 10);
//    value = isNaN(value) ? 0 : value;
//    value < 1 ? value = 1 : '';
//    value--;
//    document.getElementById('number3').value = value;
//}
//


//// Value color

/// number1
$(".number").change(function () {
    if ($(this).val() == 0) {
        $(".number").css("color", '#212121');
    } else {
        $(".number").css("color", '#c09e1d');
    }
});


$(".allac").click(function () {
    if ($(".number").val() == 0) {
        $(".number").css("color", '#212121');
    }
    //  else if ($("#number").val() > 0)   
    else {
        $(".number").css("color", '#c09e1d');
    }
});


/// number2
$(".number2").change(function () {
    if ($(this).val() == 0) {
        $(".number2").css("color", '#212121');
    } else {
        $(".number2").css("color", '#c09e1d');
    }
});


$(".allac2").click(function () {
    if ($(".number2").val() == 0) {
        $(".number2").css("color", '#212121');
    } else {
        $(".number2").css("color", '#c09e1d');
    }
});

/// number3
$(".number3").change(function () {
    if ($(this).val() == 0) {
        $(".number3").css("color", '#212121');
    } else {
        $(".number3").css("color", '#c09e1d');
    }
});


$(".allac3").click(function () {
    if ($(".number3").val() == 0) {
        $(".number3").css("color", '#212121');
    } else {
        $(".number3").css("color", '#c09e1d');
    }
});



///// Sum
$('input').keyup(function () {

    var firstValue = parseFloat($("#number").val()) || 0;
    var secondValue = parseFloat($("#number2").val()) || 0;
    var thirdValue = parseFloat($("#number3").val()) || 0;

    $('#added').html(firstValue + secondValue + thirdValue);
});

/*** End Quantity ***/



$(document).ready(function () {

    /// loading website
//    jQuery(window).load(function () {
//        $(".loader").fadeOut(500, function () {
//            $(".loading").fadeOut(500);
//            $("body").css("overflow-y", "auto");
//        });
//    });


    /** Fixed Navbar **/
    $(window).scroll(function () {
        var st = $(window).scrollTop();
        if (st > 80) {
            $(".navbar").addClass('fixd-navbar');

        } else {
            $(".navbar").removeClass('fixd-navbar');

        }
    });


    //////////////////////////////
    'use strict';

    var scrollButton,
        i,
        atr;

    scrollButton = $("#scroll-top");

    $(window).scroll(function () {
        if ($(this).scrollTop() >= 500) {
            scrollButton.show();
        } else {
            scrollButton.hide();
        }
    });


    $("#scroll-top").click(function () {
        $("html,body").animate({
            scrollTop: 0
        }, 600);
    });

    /* Start Smooth Scroll */
    $('.navbar a, .header a, .footer a, .in-filter a').click(function (e) {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });

    /* End Smooth Scroll */


    /********* Add To Cart Badge Counter ***************/
    var itemCount = 0;

    $('.add-to-cart').click(function () {
        itemCount++;
        $('#itemCount').html(itemCount).css('display', 'block');
    });

    $('.clear').click(function () {
        itemCount = 0;
        $('#itemCount').html('').css('display', 'none');
        $('#cartItems').html('');
        $(".carts").fadeOut(700);
    });

    /****************** Add To Cart Fly *************/
    $('.add-to-cart').on('click', function () {
        var cart = $('.shopping-cart');
        var imgtodrag = $(this).parents('.course1').find(".imagee").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
                }, 1000, 'easeInOutExpo');

            setTimeout(function () {
                cart.effect("shake", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
    });

    
    
    /***** Cancel order *************/
     $(".cancel").click(function (e) {
        $(this).closest(".talab1").fadeOut(500);
    });

    
    
    /// navbar button

    $("#nav-icon1").click(function () {
        $(this).toggleClass("open");
        $(".navy").toggleClass("back-nav");
        $(".body-overlay").toggleClass("back");
        $("body").toggleClass("body-over");
    });

    $(".body-overlay").click(function () {
        $(this).toggleClass("back");
        $("#nav-icon1").toggleClass("open");
        $(".navy").toggleClass("back-nav");
        $("body").toggleClass("body-over");
    });


});


//$(document).ready(function () {
//    var o;
//    jQuery(window).load(function () {
//        $(".loader").fadeOut(500, function () {
//            $(".loading").fadeOut(500), $("body").css("overflow-y", "auto")
//        })
//    }), $(".navbar a, .header a, .footer a, .in-filter a").click(function (o) {
//        $("html, body").animate({
//            scrollTop: $($(this).attr("href")).offset().top
//        }, 1e3), o.preventDefault()
//    }), $(window).scroll(function(){$(window).scrollTop()>80?$(".navbar").addClass("fixd-navbar"):$(".navbar").removeClass("fixd-navbar")
//   }),$("#nav-icon1").click(function () {
//        $(this).toggleClass("open"), $(".navy").toggleClass("back-nav"), $(".body-overlay").toggleClass("back"), $("body").toggleClass("body-over")
//    }), $(".body-overlay").click(function () {
//        $(this).toggleClass("back"), $("#nav-icon1").toggleClass("open"), $(".navy").toggleClass("back-nav"), $("body").toggleClass("body-over")
//    }), o = $("#scroll-top"), $(window).scroll(function () {
//        $(this).scrollTop() >= 500 ? o.show() : o.hide()
//    }), $("#scroll-top").click(function () {
//        $("html,body").animate({
//            scrollTop: 0
//        }, 600)
//    })
//});
