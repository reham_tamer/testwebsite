<?php

namespace App\Http\Controllers\Website;
use App\Models\Cart_items;
use App\Models\Course;
use App\Models\RequestCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelPayfort\Facades\Payfort;


class CartController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    public function addtocart($id)
    {
        dd($id);


    }

    public function pay(Request $request)
    {
        $session_id = session()->getId();
        $course_id = $request->product_id;
        $course = Course::findOrFail($course_id);
        $price = ($course->fees) - ($course->fees * $course->discount / 100);

        $p = new Cart_items();
        $p->product_id = $course_id;
        $p->amount = 1;
        $p->price = $price;
        $p->session_id = $session_id;

        $save = $p->save();
        return response()->json($save);

    }

    public function shoppingCart()
    {
        $items = Cart_items::where('session_id', session()->getId())->get();
        return view('website.shopping_cart', compact('items'));

    }
    public function invoice()
    {
        $items = Cart_items::where('session_id', session()->getId())->get();
        return view('website.invoice_details', compact('items'));

    }
    public function confirmation(Request $request)
    {
        $request->session()->regenerate();
        return view('website.confirmation' );

    }
    public function addItem($id)
    {
            $pro=Cart_items::where('product_id', $id)
                ->where('session_id', session()->getId())
                ->first();

        if (isset($pro)) {
            $pro->update([
                'price' => $pro['price'],
                'amount' => $pro['amount'] + 1,
                'product_id' => $id,
                'session_id' => session()->getId()
            ]);
            return response()->json(['pro' => $pro]);
       }

        }


    public function minusItem($id)
    {
        $pro=Cart_items::where('product_id', $id)
            ->where('session_id', session()->getId())
            ->first();
        if (isset($pro) && $pro['amount'] > 1) {
            $pro->update([
                'price' => $pro['price'],
                'amount' => $pro['amount'] + 1,
                'product_id' => $id,
                'session_id' => session()->getId()
            ]);
            return response()->json(['status' => true, 'pro' => $pro]);

        } else {
            if (isset($pro) && $pro['amount'] == 1) {
                $pro->delete();
                    return response()->json(['status' => true, 'data' => 'cart has removed']);
            }
            return response()->json(['status' => true, 'data' => 'product deleted']);
        }
    }

    public function deleteItem($id)
    {
        $cartItem = CartItem::find($id);
        $cartItem->delete();
            $cartItem->delete();
//        popup('delete');
        return back()->with('alert', 'تم الحذف بنجاح');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
//          return $request;
        $this->validate($request,[
            'total'=>'required',
            'radio'=>'required',
            'email'=>'required',
            'user_name'=>'required',
            'job'=>'required',
            'phone'=>'required',
            'company'=>'required'
        ]);
        $total=$request->input('total');
        $p = new RequestCourse();
        $p->email = $request->input('email');
        $p->user_name = $request->input('user_name');
        $p->job = $request->input('job');
        $p->phone = $request->input('phone');
        $p->company = $request->input('company');
        $p->session_id = session()->getId();

        $save = $p->save();
        $order_id=$p->id;
        if($request->input('radio')==1) {

            return Payfort::redirection()->displayRedirectionPage([
                'command' => 'AUTHORIZATION',              # AUTHORIZATION/PURCHASE according to your operation.
                'merchant_reference' => $order_id . '.34562',   # You reference id for this operation (Order id for example).
                'amount' => $total,                           # The operation amount.
                'currency' => 'SAR',                       # Optional if you need to use another currenct than set in config.
                'return_url ' => 'en.bltc.com.sa',
                'customer_email' => $p->email                # Customer email.
            ]);

        }
        else
            return redirect('/confirmation/');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q =Cart_items::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'تم حذف الدورة التدريبية من السلة بنجاح',
            'id' => $id
        ];
        return response()->json($data, 200);
    }

}

?>