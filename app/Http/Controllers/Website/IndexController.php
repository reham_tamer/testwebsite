<?php

namespace App\Http\Controllers\Website;
use App\Models\Course;
use App\Models\Category;
use App\Models\Slider;
use App\Models\Client;
use App\Models\Partener;
use App\Models\Course_City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class IndexController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $courses=Course::where('status',0)->orderBy('id', 'desc')->simplePaginate(25);
        $categories=Category::all();
        $slider=Slider::all();
        $clients=Client::all();
        $partners=Partener::all();
        $months = Course_City::orderBy('from','desc')->get();
        return view('website.main',compact('courses','categories','slider','months','clients','partners'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

    }

}

?>