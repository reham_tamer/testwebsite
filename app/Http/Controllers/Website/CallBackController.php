<?php

namespace App\Http\Controllers\Website;
use App\Models\CallMeBack;
use App\Models\Category;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CallBackController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('website.call_back',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'last_name'=>'required',
            'first_name'=>'required',
            'company'=>'required',
            'phone'=>'required'
        ]);

        $inputs=$request->all();
        CallMeBack::create($inputs);

        return back()->withFlashMessage(' your data  sent successfully ');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

    }

}

?>