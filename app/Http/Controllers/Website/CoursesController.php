<?php

namespace App\Http\Controllers\Website;
use App\Models\Blog;
use App\Models\City;
use App\Models\Client;
use App\Models\Instructor;
use App\Models\MainService;
use App\Models\Slider;
use Carbon\Carbon;
use App\Models\ContactUs;
use App\Models\Course;
use App\Models\Category;
use App\Models\Course_City;
use App\Models\Partener;
use App\Models\RequestCourse;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelPayfort\Facades\Payfort;


class CoursesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $courses=Course::where('status',0)->orderBy('id', 'desc')->paginate(18);
        $categories=Category::all();
        $clients=Client::all();
        $partners=Partener::all();
        $slider=Slider::all();
        return view('website.main',compact('courses','categories','slider','partners','clients'));

    }

    public function courseSingle($id)
    {
        $course=Course::find($id);
        $categories=Category::all();
        $cities=Course_City::where('course_id',$id)->get();
        return view('website.course_single',compact('course','categories','cities'));

    }
    public function coursedetails($id)
    {
        $course=Course::find($id);
        return view('website.course_details',compact('course'));

    }
    public function availableCourses()
    {
        $courses=Course::orderBy('id', 'desc')->get();
        $categories=Category::all();
        return view('website.available_courses',compact('courses','categories'));

    }
    public function allCourses()
    {
        $courses=Course::orderBy('id', 'desc')->get();
        $categories=Category::all();
        $cities=City::all();
        $months = Course_City::orderBy('from','desc')->get()->groupBy(function($d) {
            return Carbon::parse($d->from)->format('m-y');
        });
//        dd($months);

        return view('website.courses',compact('courses','categories','months','cities'));

    }
    public function corporate()
    {
        $courses=Course::orderBy('id', 'desc')->get();
        $categories=Category::all();
        $cities=City::all();
        $months = Course_City::orderBy('from','desc')->get()->groupBy(function($d) {
            return Carbon::parse($d->from)->format('m-y');
        });
//        dd($months);

        return view('website.corporate',compact('courses','categories','months','cities'));

    }
    public function publicc()
    {
        $courses=Course::orderBy('id', 'desc')->get();
        $categories=Category::all();
        $cities=City::all();
        $months = Course_City::orderBy('from','desc')->get()->groupBy(function($d) {
            return Carbon::parse($d->from)->format('m-y');
        });
//        dd($months);

        return view('website.public',compact('courses','categories','months','cities'));

    }
    public function elearning()
    {
        $courses=Course::orderBy('id', 'desc')->get();
        $categories=Category::all();
        $cities=City::all();
        $months = Course_City::orderBy('from','desc')->get()->groupBy(function($d) {
            return Carbon::parse($d->from)->format('m-y');
        });
//        dd($months);

        return view('website.elearning',compact('courses','categories','months','cities'));

    }
    public function weekend()
    {
        $courses=Course::orderBy('id', 'desc')->get();
        $categories=Category::all();
        $cities=City::all();
        $months = Course_City::orderBy('from','desc')->get()->groupBy(function($d) {
            return Carbon::parse($d->from)->format('m-y');
        });
        return view('website.weekend',compact('courses','categories','months','cities'));
    }
    public function search(Request $request)
    {//        dd($request);
        $categories=Category::all();
        $cities=City::all();
        $cat_id=$request->cat_id;
        $months = Course_City::orderBy('from','desc')->with(['course' => function($query)use($cat_id) {
            $query->Where('category_id', $cat_id);
        }])->get()->groupBy(function($d) {
        return Carbon::parse($d->from)->format('m-y');
    });
        return view('website.courses',compact('categories','months','cities'));
    }
    public function searchBlogs(Request $request)
    {//        dd($request);
        $item=$request->item;
       $blogs=Blog::Where('title_ar','like', $item)
           ->orWhere('body_ar','like',$item)->get();
        return view('website.blogs',compact('blogs'));
    }
    public function showService($id)
    {
        $service=Service::where('main_service_id',$id)->get();
        return view('website.service',compact('service'));

    }
    public function showSubService($id)
    {
        $service=Service::where('id',$id)->first();
        return view('website.subservice',compact('service'));

    }
    public function showMainService()
    {
        $service=MainService::all();
        return view('website.mainservice',compact('service'));

    }
 public function showBlogs()
{
    $blogs=Blog::all();
    return view('website.blogs',compact('blogs'));

}
    public function showSingleBlog($id)
    {
        $blog=Blog::findorfail($id);
        return view('website.single_blog',compact('blog'));

    }
    public function trainingMethodology()
    {
        return view('website.training-methodology');

    }
    public function aboutUs()
    {
        return view('website.about_us');

    }

    public function terms()
    {
        return view('website.Terms');

    }
    public function privacy()
    {
        return view('website.privacy');

    }
    public function cancellation()
    {
        return view('website.cancellation');

    }
    public function ourPartner()
    {
        $partners=Partener::all();
        return view('website.our_partners',compact('partners'));

    }
    public function Methodology()
    {

    }
    public function allClients()
    {
        $clients=Client::all();
        return view('website.our_clients',compact('clients'));

    }

    public function Trainers()
    {
        $trainers=Instructor::all();
        return view('website.trainers',compact('trainers'));

    }




    public function allinCategory($id)
    {
        $course=Course::where('category_id',$id)->get();
        $categories=Category::all();

        return view('website.all_in_category',compact('course','categories'));
    }
    public function registerSingle($id)
    {
        $course=Course::findOrfail($id);
        $cities=Course_City::where('course_id',$id)->get();
        return view('website.register_single_course',compact('course','cities'));
    }
    public function registerAll()
    {
        $categories=Category::pluck('name','id')->toArray();
        return view('website.register_courses',compact('categories'));
    }
    public function ajaxCategory($id)
    {
        $categories=Course::where('category_id', $id)->where('status',0)->get();
        return response()->json($categories);
    }
    public function ajaxCourse($id)
    {
        $cities=Course_City::with('city')->where('course_id', $id)->get();
        return response()->json($cities);
    }
    public function ajaxCourseDetails($id)
    {
        $course=Course::where('id', $id)->get();
        return response()->json($course);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'course_id'=>'required',
            'city_date_id'=>'required',
            'category_id'=>'required',
            'email'=>'required',
            'user_name'=>'required',
            'job'=>'required',
            'phone'=>'required',
            'company'=>'required'
        ]);
        $p =RequestCourse::create($request->all());
        $order_id=$p->id;

        $course=Course::findOrfail($request->course_id);
        $price=$course->fees;

        return Payfort::redirection()->displayRedirectionPage([
            'command' => 'AUTHORIZATION',              # AUTHORIZATION/PURCHASE according to your operation.
            'merchant_reference' => $order_id.'.34562',   # You reference id for this operation (Order id for example).
            'amount' => $price,                           # The operation amount.
            'currency' => 'SAR',                       # Optional if you need to use another currenct than set in config.
            'return_url '=> 'en.bltc.com.sa',
            'customer_email' => $p->email                # Customer email.
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

    }

}

?>