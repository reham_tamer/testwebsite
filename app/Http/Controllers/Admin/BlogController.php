<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $blogs=Blog::all();
        return view('admin.blogs.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.blogs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title_ar'=>'required',
            'title_en'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
            'date'=>'required',
            'image'=>'required'
        ]);
        $inputs=$request->all();

        Blog::create($inputs);
        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $inputs->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Blog added successfully ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $blogs=Blog::findOrFail($id);
        return view('admin.blogs.edit',compact('blogs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $blogs = Blog::findOrFail($id);

        $blogs->update($request->all());
        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $blogs->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Blog  updated successfully ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = Blog::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'Blog deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }

}

?>