<?php

namespace App\Http\Controllers\Admin;

use App\Models\Course_City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseCitiesController  extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }
    public function all($id)
    {
        $course_id=$id;
        $cities=Course_City::where('course_id',$id)->get();
        return view('admin.courses.cities',compact('cities','course_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.courses.add_city');
    }
    public function createcity($id)
    {
        return view('admin.courses.add_city',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'course_id'=>'required',
            'city_id'=>'required',
            'from'=>'required',
            'to'=>'required'
        ]);

        $inputs=$request->all();
        Course_City::create($inputs);
        return back()->withFlashMessage('city added successfully ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $city=Course_City::findOrFail($id);
        return view('admin.courses.edit_city',compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [

        ]);
        $city = Course_City::findOrFail($id);

        $city->update($request->all());
        return back()->withFlashMessage('city updated successfuly ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = Course_City::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'city deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }

}

?>