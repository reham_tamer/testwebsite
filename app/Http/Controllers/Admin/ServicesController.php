<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class ServicesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $services=Service::all();
        return view('admin.services.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.services.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image'=>'required',
            'name'=>'required',
            'name_ar'=>'required',
            'body_ar'=>'required',
            'body'=>'required',
            'image'=>'required',
            'main_service_id'=>'required'
        ]);
        $services = Service::create($request->all());

        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $services->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Service added successfully ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $services=Service::findOrFail($id);
        return view('admin.services.edit',compact('services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $services = Service::findOrFail($id);

        $services->update($request->all());
        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $services->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Service updated successfully ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = Service::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'Service deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }

}

?>