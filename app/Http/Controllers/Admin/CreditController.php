<?php

namespace App\Http\Controllers\Admin;

use App\Models\Credit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class CreditController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $credits=Instructor::all();
        return view('admin.credits.index',compact('credits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.credits.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image'=>'required',
            'name_ar'=>'required',
            'name_en'=>'required'
        ]);
        $credits = Credit::create($request->all());

        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $credits->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('credit added successfully ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $credits =Credit::findOrFail($id);
        return view('admin.credits.edit',compact('credits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

        $credits = Credit::findOrFail($id);

        $credits->update($request->all());
        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $credits->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('credit updated successfully ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = Credit::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'credit deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }

}

?>