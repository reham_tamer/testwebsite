<?php

namespace App\Http\Controllers\Admin;

use App\Models\Partener;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class PartnerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $partner=Partener::all();
        return view('admin.partners.index',compact('partner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.partners.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image'=>'required'
        ]);
        $Partener = Partener::create($request->all());

        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $Partener->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Partner added successfully ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $partner =Partener::findOrFail($id);
        return view('admin.partners.edit',compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

        $Partener = Partener::findOrFail($id);

        $Partener->update($request->all());
        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $Partener->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Partner updated successfully ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = Partener::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'Partner deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }

}

?>