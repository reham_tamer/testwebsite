<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $cities=City::all();
    return view('admin.cities.index',compact('cities'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('admin.cities.add');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $this->validate($request,[
          'name'=>'required|string|'
      ]);

      $inputs=$request->all();
      City::create($inputs);
      return back()->withFlashMessage('city added successfully ');
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      $city=City::findOrFail($id);
      return view('admin.cities.edit',compact('city'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id, Request $request)
  {
      $this->validate($request, [
          'name' => 'required'

      ]);
      $city = City::findOrFail($id);

      $city->update($request->all());
      return back()->withFlashMessage('city updated successfuly ');

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $q = City::findOrFail($id);
      $q->delete();
      $data = [
          'status' => 1,
          'msg' => 'city deleted successfully',
          'id' => $id
      ];
      return response()->json($data, 200);


  }
  
}

?>