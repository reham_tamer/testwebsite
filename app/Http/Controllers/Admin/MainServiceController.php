<?php

namespace App\Http\Controllers\Admin;

use App\Models\MainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainServiceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $services=MainService::all();
        return view('admin.main_services.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.main_services.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'ar_name'=>'required',
            'en_name'=>'required',
            'body_en'=>'required',
            'body_ar'=>'required',
            'image'=>'required'
        ]);
        $inputs=$request->all();

        MainService::create($inputs);
        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $inputs->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Main Service added successfully ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $services=MainService::findOrFail($id);
        return view('admin.main_services.edit',compact('services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $services = MainService::findOrFail($id);

        $services->update($request->all());
        if ($request->hasFile('image'))
        {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $services->update(['image' => 'images/' . $name]);
        }
        return back()->withFlashMessage('Main Service updated successfully ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = MainService::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'Service deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }

}

?>