<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $slider=Slider::all();
      return view('admin.slider.index',compact('slider'));
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      return view('admin.slider.add');

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $this->validate($request,[
          'image'=>'required'
      ]);
      $slider = Slider::create($request->all());

      if ($request->hasFile('image'))
      {
          $img = $request->file('image');
          $destinationPath = base_path() . '/images';
          $extension = $img->getClientOriginalExtension(); // getting image extension
          $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
          $img->move($destinationPath, $name); // uploading file to given
          $slider->update(['image' => 'images/' . $name]);
      }
      return back()->withFlashMessage('Slider added successfuly ');


  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $q = Slider::findOrFail($id);
      $q->delete();
      $data = [
          'status' => 1,
          'msg' => 'Image deleted successfully',
          'id' => $id
      ];
      return response()->json($data, 200);
    
  }
  
}

?>