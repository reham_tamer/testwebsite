<?php

namespace App\Http\Controllers\Admin;

use App\Models\CallMeBack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CallMeBackController  extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $call=CallMeBack::all();
      return view('admin.call_me.show',compact('call'));
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $q = CallMeBack::findOrFail($id);
      $q->delete();
      $data = [
          'status' => 1,
          'msg' => 'Message deleted successfully',
          'id' => $id
      ];
      return response()->json($data, 200);

  }
  
}

?>