<?php

namespace App\Http\Controllers\Admin;

use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class CourseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $courses=Course::all();
        return view('admin.courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.courses.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      
        $this->validate($request,[
            'name'=>'required|string|',
            'days_num'=>'required',
            'language'=>'required',
            'image'=>'required',
            'category_id'=>'required'
            
        ]);
     $Course = Course::create($request->all());

      if ($request->hasFile('image'))
      {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $Course->update(['image' => 'images/' . $name]);
}
        return back()->withFlashMessage('Course added successfuly ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $course=Course::findOrFail($id);
        return view('admin.courses.edit',compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'

        ]);
        $Course = Course::findOrFail($id);

        $Course->update($request->all());
         if ($request->hasFile('image'))
      {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $Course->update(['image' => 'images/' . $name]);
}
        return back()->withFlashMessage('Course updated successfuly ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = Course::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'Course deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }
        public function approve($id)
    {
        $course = Course::findOrFail($id);
        $course->status = '1';
        $course->save();
        return back()->withFlashMessage('Course activated successfuly ');
    }


    public function unApprove($id)
    {
        $course = Course::findOrFail($id);
        $course->status = '0';
        $course->save();
        return back()->withFlashMessage('Course  deactivated ');
    }
    public function cities($id)
    {
             return view('admin.courses.cities',['id'=>$id]);
    }
    public function dates($id)
    {
        return view('admin.courses.dates',['id'=>$id]);
    }

}

?>