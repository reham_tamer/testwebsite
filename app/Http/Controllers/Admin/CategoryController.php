<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories=Category::all();
        return view('admin.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|',
            'image'=>'required'
        ]);
     $category = Category::create($request->all());

      if ($request->hasFile('image'))
      {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $category->update(['image' => 'images/' . $name]);
}
        return back()->withFlashMessage('Category added successfuly ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $courses=Course::where('category_id',$id)->get();
        return view('admin.categories.show',compact('courses'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category=Category::findOrFail($id);
        return view('admin.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'

        ]);
        $category = Category::findOrFail($id);

        $category->update($request->all());
         if ($request->hasFile('image'))
      {
            $img = $request->file('image');
            $destinationPath = base_path() . '/images';
            $extension = $img->getClientOriginalExtension(); // getting image extension
            $name = time() . '' . rand(11111, 99999) . '.' . $extension; // renameing image
            $img->move($destinationPath, $name); // uploading file to given
            $category->update(['image' => 'images/' . $name]);
}
        return back()->withFlashMessage('Category updated successfuly ');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $q = Category::findOrFail($id);
        $q->delete();
        $data = [
            'status' => 1,
            'msg' => 'Category deleted successfully',
            'id' => $id
        ];
        return response()->json($data, 200);


    }

}

?>