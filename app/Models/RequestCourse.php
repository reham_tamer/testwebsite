<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestCourse extends Model 
{

    protected $table = 'request_course';
    public $timestamps = true;
    protected $fillable = array('course_id', 'city_date_id','session_id', 'category_id', 'email', 'user_name', 'job', 'phone', 'company');
    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
    public function cartItems(){
        return $this->hasMany(Cart_items::class,'session_id');
    }
    public function courses()
    {
        return $this->belongsTo('App\Models\Course', 'course_id');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\Course_City', 'city_date_id');
    }

}