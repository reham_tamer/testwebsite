<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    public $timestamps = true;
    protected $fillable = array('name_ar','language_ar','why_attend_ar','course_methodology_ar',
        'course_objective_ar','target_audience_ar','course_outline_ar',
        'name', 'days_num', 'language', 'why_attend', 'course_methodology', 'course_objective',
        'target_audience', 'fees', 'course_outline', 'status', 'category_id', 'image', 'discount',
        'instructor_id','credit_id','type');

    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id')->withDefault([
            'name'=>'not found'
        ]);

    }
    public function instructors()
    {
        return $this->belongsTo('App\Models\Instructor', 'instructor_id')->withDefault([
            'name_en'=>'not found'
        ]);

    }

    public function credits()
    {
        return $this->belongsTo('App\Models\Credit', 'credit_id')->withDefault([
            'name_en'=>'not found'
        ]);

    }

    
}