<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $table = 'blogs';
    public $timestamps = true;
    protected $fillable = array('title_ar', 'title_en','body_en','body_ar','image','tags','date');


}