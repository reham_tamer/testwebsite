<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{

    protected $table = 'credits';
    public $timestamps = true;
    protected $fillable = array('name_en', 'body_en','name_ar','body_ar','image');


}