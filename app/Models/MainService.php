<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainService extends Model
{

    protected $table = 'main_service';
    public $timestamps = true;
    protected $fillable = array('ar_name', 'en_name','body_en','body_ar','image');


}