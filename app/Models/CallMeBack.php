<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallMeBack extends Model 
{

    protected $table = 'call_me_back';
    public $timestamps = true;
    protected $fillable = array('first_name', 'last_name', 'company', 'phone');

}