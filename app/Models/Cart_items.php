<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart_items extends Model
{

    protected $table = 'cart_items';
    public $timestamps = true;
    protected $fillable = array('product_id', 'amount','price','session_id');

    public function courses()
    {
        return $this->belongsTo('App\Models\Course', 'product_id');

    }

}