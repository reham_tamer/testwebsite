<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $table = 'services';
    public $timestamps = true;
    protected $fillable = array('name', 'body','main_service_id','name_ar','body_ar','image');

    public function service()
    {
        return $this->belongsTo('App\Models\MainService', 'main_service_id')->withDefault([
            'en_name'=>'not found'
        ]);
    }


}