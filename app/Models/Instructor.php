<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{

    protected $table = 'instructors';
    public $timestamps = true;
    protected $fillable = array('name_en', 'body_en','name_ar','body_ar','image','link');


}