<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course_City extends Model
{

    protected $table = 'course_city';
    public $timestamps = true;
    protected $fillable = array('course_id','city_id','from','to');
    public function course()
    {
        return $this->belongsTo('App\Models\Course', 'course_id')->withDefault([
            'name'=>'not found',
            'from'=>'No Dates Available',
            'to'=>'No Dates Available'
        ]);;
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id')->withDefault([
            'name'=>'not found'
        ]);
    }


}