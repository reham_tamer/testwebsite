<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Date extends Model 
{

    protected $table = 'dates';
    public $timestamps = true;
    protected $fillable = array('from', 'to', 'course_id');

    public function cources()
    {
        return $this->belongsTo('App\Models\Course', 'cource_id');
    }

}