<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Region $region
 * @property-read \App\SubCategory $sub_category
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */


    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */

    protected $table = 'users';
    public $timestamps = true;
    protected $fillable = array('user_name', 'email', 'password');

}