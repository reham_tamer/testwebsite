<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <title> BLTC</title>
    <link rel="stylesheet" href="{{asset('public/website/css/bootstrap.min.css')}}">
    <link rel="shortcut icon" href="{{asset('public/website/img/BLTC-favicon.png')}}">
    <link rel="stylesheet" href="{{asset('public/website/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/website/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('public/website/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/website/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('public/website/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('public/website/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/admin/assets/sweetalert2.min.css')}}">


    <!------ IE -------->
    <script src="{{asset('public/website/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('public/website/js/respond.min.js')}}"></script>

</head>



<body>
<div class="body-overlay"></div>
<!-- Start Loading-Page -->
<!--
<div class="loader">
    <div class="in-load">
        <div>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>
-->
<!-- End Loading-Page -->


<!-- Start Top Nav -->
<section class="top-nav">
    <div class="row">

        <div class="col-sm-6 col-xs-8">
            <div class="topnav-right">
                <ul class="social">
                    <li>
                        <a href="https://www.facebook.com/BLTCompany/">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/bltc_sa">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/business-leadership-training-co/">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                </ul>

                <a href="#" class="lang">English</a>

            </div>
        </div>

        <div class="col-sm-6 col-xs-4">
            <div class="topnav-left">
                <a href="mailto:bltc@example.com">
                    <span class="hidden-xs">ccs@bltc.com.sa </span>
                    <span class="call-icon"><i class="fas fa-envelope"></i></span>
                </a>
                <a href="tel:+96592486">
                    <span class="hidden-xs">00966556001307 </span>
                    <span class="call-icon"><i class="fas fa-phone"></i></span>
                </a>
            </div>
        </div>

    </div>
</section>
<!-- End Top Nav -->


<!--Start Navbar-->
<div class="navbar">
    <div class="row no-margin">

        <div class="col-md-2 col-sm-2 col-xs-6 no-padding">
            <div class="nav-right">
                <a href="{{url('/')}}" class="logo-nav">
                    <img src="{{asset('public/website/img/logo.png')}}">
                </a>
            </div>
        </div>

        <div class="col-md-8 col-sm-8 col-xs-0 no-padding">
            <div class="navy">
                <ul class="nav cf" id="ul1">

                    <li {{ Request::is('/') ? 'class=active' : '' }}><a href="{{url('/')}}">الرئيسية</a></li>
                    <!----Start Mega Menu --->
                    <li class="dropdown">
                        <button class="dropbtn">دورات تدريبية
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <div class="dropdown-content">
                                <a href="{{url('/available_courses/')}}"> <span><i class="fas fa-user-graduate"></i></span> كل الدورات</a>
                            <div class="row">

                                    <?php
                                $categories=App\Models\Category::all();
                                ?>
                                @foreach($categories->chunk(4) as $chunk)
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="column">
                                        @foreach($chunk as $item)
                                                <a href="{{url('/all_in_category/'.$item->id)}}">
                                                    <span class="mega-i"><i class="fas fa-user-graduate"></i></span>
                                                    {{$item->name_ar}}</a>

                                                {{--Item {{ $loop->iteration }}--}}
                                        @endforeach
                                        </div>

                                    </div>
                                @endforeach

                            </div>
                        </div>
                    <!----End Mega Menu --->

            </li>

                    <li {{ Request::is('all_courses') ? 'class=active' : '' }}><a href="{{url('/all_courses/')}}">الجدول الزمنى</a></li>
                    <li {{ Request::is('showmainservices') ? 'class=active' : '' }}><a href="{{url('/showmainservices/')}}">الاستشارات</a></li>
                    <li {{ Request::is('blogs') ? 'class=active' : '' }}><a href="{{url('/blogs')}}"> المدونة</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">من نحن
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('/about_us/')}}">من نحن </a></li>
                            <li><a href="{{url('/our_partner/')}}">شركاؤنا</a></li>
                            <li><a href="{{url('/all_clients/')}}">عملاؤنا</a></li>
                            <li><a href="{{url('/training_methodology/')}}">منهجية التدريب</a></li>

                        </ul>
                    </li>
                    <li {{ Request::is('/contact/create') ? 'class=active' : '' }}><a href="{{url('/contact/create')}}">تواصل معنا</a></li>
            </ul>
                </div>
        </div>
        

        <div class="col-sm-2 col-xs-3">
            <div title="عربة التسوق" class="badge-li">
                <span class="badge" id="itemCount">
                    {{count(App\Models\Cart_items::where('session_id',session()->getId())->get())}}
                </span>
                <a class="shopping-cart" href="{{url('/shopping-cart/')}}">
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </div>
        </div>

        <div class="col-sm-0 col-xs-3 no-padding">
            <div id="nav-icon1">
                <span> </span>
                <span> </span>
                <span> </span>
            </div>
        </div>
</div>

    </div>


@yield('content')
@include('sweet::alert')

<section class="footer">
    <div class="container">

        <div class="footer-content">

            <div class="row">

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="foot1">
                        <div class="new-logo">
                            <img src="{{url('public/website/img/logo-lg.png')}}">
                        </div>

                        <p>
                            {{--{{\Carbon\Carbon::now()->formatLocalized('%m %B')}}--}}
                            بناء شراكات مهنية محلية ودولية تنقل إلى الأسواق المحلية والإقليمية اخر التطورات فى صناعة التدريب.
                        </p>

                        <a href="apply.html" class="btn-3">
                            سجل الان
                       </a>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="foot1">
                        <h3>تواصل معنا</h3>
                        <p>
                            للتواصل معنا لأى استفسار أو مشكلة . فإن دعمنا متوفر لمساعدتك من الأحد إلى الخميسمن الساعة 9 صباحا إلى 4 مساءا
                        </p>

                        <ul>
                            <li>
                                <a href="works.html">
                                    <i class="fas fa-map-marker"></i>
                                    P.O Box 7495 Riyadh 13211
                                    2nd floor,Al jazeerah Bank Building,Khrais Road, Riyadh, Saudia Arabia                                </a>
                            </li>
                            <li>
                                <a href="tel:+966 112326362">
                                    <i class="fas fa-phone"></i>
                                    Phone: +966 112326362
                                </a>
                            </li>
                            <li>
                                <a href="tel:+966 112326361">
                                    <i class="fas fa-phone"></i>
                                    Fax: +966 112326361
                                </a>
                            </li>
                            <li>
                                <a href="mailto:css@bltc.com.sa">
                                    <i class="fas fa-envelope"></i>
                                    css@bltc.com.sa
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="foot1">
                        <h3> روابط تهمك </h3>
                        <ul class="inline-xs">
                            <li>
                                <a href="{{url('/available_courses/')}}">
                                    دورات تدريبية
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/all_courses/')}}">
                                    الجدول الزمنى
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/about_us')}}">
                                    من نحن
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/contact/create')}}">
                                    تواصل معنا
                                </a>
                                </li>
                            <li><a href="{{url('/terms/')}}">الشروط والأحكام </a></li>
                            <li><a href="{{url('/privacy/')}}">سياسة الخصوصية</a></li>
                            <li><a href="{{url('/cancellation/')}}">سياسة الالغاء</a></li>
                        </ul>

                        <ul class="social foot-social">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>

                    </div>

                </div>




            </div>


        </div>
    </div>


    <div class="copyright">
        <span>&copy;</span> 2019 جميع الحقوق محفوظة
    </div>

</section>


<!--Scroll Button-->
<div id="scroll-top">
    <i class="fa fa-angle-up"></i>
</div>



<!-- Strat End -->
<!--===============================
     SCRIPT
     ===================================-->
<script src="{{asset('public/website/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('public/website/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('public/admin/assets/jquery-confirm/jquery.confirm.min.js')}}"></script>

<script src="{{asset('public/website/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/website/js/all.min.js')}}"></script>

<!----------------- These for here calender search Only ----------------->
<!--- select2 --->
<script src="{{asset('public/website/js/select2.full.js')}}"></script>
<script src="{{asset('public/website/js/script.js')}}"></script>
<script src="{{asset('public/website/js/owl.carousel.min.js')}}"></script>


<script>
    $(document).ready(function() {
        $(".one-select2").select2({
            placeholder: "التخصص"
        });
        $(".two-select2").select2({
            placeholder: "المدينة"
        });
        $(".three-select2").select2({
            placeholder: "اللغة"
        });
        $(".four-select2").select2({
            placeholder: "الشهر"
        });
        $(".five-select2").select2({
            placeholder: "السنة"
        });
    });

</script>


<!--- Owl Carousel --->

@yield('script')

<!--- Owl Carousel --->
<script>
    $(document).on('click', '.destroy', function () {
        var route = $(this).data('route');
        var token = $(this).data('token');
        $.confirm({
            icon: 'glyphicon glyphicon-floppy-remove',
            animation: 'rotateX',
            closeAnimation: 'rotateXR',
            title: 'تأكيد عملية الحذف',
            autoClose: 'cancel|6000',
            text: 'هل أنت متأكد من الحذف ؟',
            confirmButtonClass: 'btn-outline',
            cancelButtonClass: 'btn-outline',
            confirmButton: 'نعم',
            cancelButton: 'لا',
            dialogClass: "modal-danger modal-dialog",
            confirm: function () {
                $.ajax({
                    url: route,
                    type: 'post',
                    data: {_method: 'delete', _token: token},
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 0) {
                            //toastr.error(data.msg)
                            swal("خطأ!", data.msg, "error")
                        } else {
                            $("#removable" + data.id).remove();
                            swal("Good!", data.msg, "success")
                            //toastr.success(data.msg)
                        }
                    }
                });
            },
        });

    });


    $("#owl-demo , #owl-training").owlCarousel({
        rtl: true,
        loop: true,
        autoplay: false,
        items: 1,
        dots: true,
        autoplayHoverPause: false,
        animateOut: 'flipOutX',
        animateIn: 'flipInX',
        nav: true,
        navText: [
            '<i class="fas fa-angle-right"></i>',
            '<i class="fas fa-angle-left"></i>'
        ],
        rewindNav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            991: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

</script>
<script>
    $("#owl-types").owlCarousel({
        rtl: true,
        autoplay: false,
        autoplayTimeout: 2500,
        autoplayHoverPause: true,
        loop: true,
        itemsDesktop: [1199, 6],
        itemsDesktopSmall: [979, 3],
        nav: true,
        navText: ["<i class='fas fa-caret-right'>", "<i class='fas fa-caret-left'>"],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 6
            }
        }
    });

</script>

<script>
    $("#owl-clients").owlCarousel({
        rtl: true,
        autoplay: false,
        autoplayTimeout: 2500,
        autoplayHoverPause: true,
        loop: true,
        nav: false,
        dots: true,
        responsive: {
            0: {
                dotsEach: 2,
                items: 1
            },
            600: {
                dotsEach: 3,
                items: 3
            },
            1000: {
                dotsEach: 5,
                items: 4
            }
        }
    });

</script>

<script>
    $("#owl-partners").owlCarousel({
        rtl: true,
        autoplay: false,
        autoplayTimeout: 2500,
        autoplayHoverPause: true,
        loop: true,
        nav: false,
        dots: true,
        responsive: {
            0: {
                dotsEach: 2,
                items: 1
            },
            600: {
                dotsEach: 3,
                items: 3
            },
            1000: {
                dotsEach: 5,
                items: 4
            }
        }
    });

</script>

<!--- Dynamic Height ---->
<script>
    $(document).ready(function() {
        var divHeight = $('.training').height();
        $('.train-blu').css('min-height', divHeight + 'px');
    });

</script>


<script>
    $("#category_id").change(function () {
        var category_id = $("#category_id").val();

        $.ajax({
            type: 'get',
            url: '{{url('/ajax_category')}}/' + category_id,
            success: function (data) {
                $.each(data, function (key, value) {
                    var option = '<option value="' + value.id + '">' + value.name + '</option>';
                    $("#course_id").append(option);
                });
            },
            error: function () {

            }
        });
    });


    $("#course_id").change(function () {
        var course = $("#course_id").val();
        $.ajax({
            type: 'get',
            url: '{{url('/ajax_course')}}/' + course,
            success: function (data) {
                $.each(data, function (key, val) {
                var markup =
                    "<tr>" +
                    "<td><input type='radio' value='" + val.id + "' name='city_date_id'></td>" +
                    "<td>" + val.city.name + "</td>" +
                    "<td>" + val.from +"</td>" +
                    "<td>" + val.to +"</td>" +
                    " </tr>";
                    $("#cities").append(markup);
                 })
            },
            error: function () {

            }
        });

    });


    $('.course').change(function () {
        var course = $("#course_id").val();
        $.ajax({
            type: 'get',
            url: '{{url('/ajax_course_datails')}}/' + course,
            success: function (data) {
                $.each(data, function (key, val) {
                    console.log(data);
                    var f = val.fees;
                    var d = val.days_num;
                    var l = val.language;
                    console.log(d);
                    $('#duration').text('Duration : '+d);
                    $('#lang').text('Language : '+l);
                    $('#fees').text('$'+f);
                })
            }
        });

    });


</script>
<!--
        <script>
    
    jQuery(window).load(function () {
        $(".loader").fadeOut(500, function () {
            $(".loading").delay(1000).fadeOut(500);
            $("body").css("overflow-y", "auto");
        });
    });
    </script>
-->
<script src="{{asset('public/website/js/wow.min.js')}}"></script>

<script>
    new WOW().init();
</script>
<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: {
                lat: 26,
                lng: 43
            }
        });
        var image =
            'img/map-marker.png';
        var beachMarker = new google.maps.Marker({
            position: {
                lat: 26.348180,
                lng: 43.955276
            },
            map: map,
            icon: image
        });
    }
    $('.AddToCart').on('click', function (e) {

        var productId = $(this).attr('data-product-id');
        console.log(productId);
        $.ajax({
            type: 'get',
            url: '{{url('/pay')}}',
            data:{
                product_id:productId
            },
            success: function (data) {
                $.each(data, function (key, val) {
                    console.log(data);

                })
            }
        });
    })

    $('.add').on('click', function (e) {
        e.preventDefault();
        var pri,amo;
        var tot=0;
        var counttotal;
        var productId = $(this).attr('data-product-id');
        console.log(productId);
        $.ajax({
            type: 'get',
            url: '{{url('/add/cart')}}/' + productId,
            success: function (data) {
                $.each(data, function (key, val) {
                    console.log(data);
                    pri=parseInt(val.price);
                    amo=parseInt(val.amount);
                    tot=pri*amo;
                    console.log(tot);
                    $(this).parents('.talab-dtls1').find('.totalPrice').text(tot);

                });

            }

    });
    })

</script>
    
    <script>
        $(document).ready(function(){

//    update_amounts();
//    $('.qty').change(function() {
//        update_amounts();
//        
//        
//function update_amounts()
//{
//    var sum = 0.0;
//    $('.carts .talab1').each(function() {
//        var qty = $(this).find('input.number').val();
//        var price = $(this).find('.talab-dtls1 b').val();
//        var amount = (qty*price);
//        sum+=amount;
//        $(this).find('.totalPrice').text(''+amount);
//        console.log(reeeeeeeeeem);
//    });
//    $('.total').text(sum);
//}
//        
//    });



            
            
/***********************************************/
//            var total= 0;
//
//$('.carts .talab1').each(function(){
//var quantity = parseInt( $(this).find('input.number').val(),10)
//var price = parseInt( $(this).find('.talab-dtls1 b').text(),10)
//total +=  quantity * price;
//})
//alert(total);
            
            });
    </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi7RVqBGvLWMhmV_uv81zZ2Iv1ZvWOT9M&callback=initMap">
</script>
</body>

</html>