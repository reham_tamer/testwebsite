@extends('layouts.master')
@section('title')
    الجدول الزمني
@endsection
@section('content')
    @inject('dates','App\Models\course_city')

    <!----- Start Breadcrumbs ----->
    <section class="breadcrumbs">
        <b>الجدول الزمنى</b> - <span>برامج 2019</span>
    </section>
    <!----- End Breadcrumbs ----->



    <!----- Start Search ----->
    <section class="searching">
        <form action="{{url('/search')}}" class="search" method="get">

            <div class="form-width">
                <input type="text" placeholder="ابحث عن دورة تدريبية معينة" name="course_name">
            </div>

            <div class="form-width">
                <select name="cat_id" class="js-select2 one-select2">
                    <option>اختر التخصص</option>
                    @foreach($categories as $cat)
                        <option value="{{$cat->id}}">{{$cat->name_ar}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-width">
                <select name="city_id" class="js-select2 two-select2">
                    <option>اختر المدينة</option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name_ar}}</option>
                    @endforeach
                </select>
                </select>
            </div>

            <div class="form-width">
                <select name="language" class="js-select2 three-select2">
                    <option>اختر اللغة</option>
                    <option value="العربية">العربية </option>
                    <option value="english">الانجليزية</option>
                </select>
            </div>

            <button type="submit" class="form-width btn-3 srch-to"> ابحث <i class="fas fa-search"></i> </button>

        </form>
    </section>
    <!----- End Search ----->

    <!---------------------- Start Calender ---------------------->
    <section class="calender">
        <div class="container">

            <h3 class="h3-title"> أقسام الجدول الزمنى</h3>
            <!---- start filter -->
            <div class="filter">
                <a href="{{url('/all_courses/')}}"> عرض الكل </a>
                <a class="active" href="{{url('/corporate')}}"> البرامج التدريبية للأفراد </a>
                <a href="{{url('/company')}}"> البرامج التدريبية للشركات </a>
                <a href="{{url('/elearning')}}"> برامج التدريب الإلكترونى </a>
                <a href="{{url('/weekend')}}"> برامج نهاية الأسبوع المخفضة </a>
            </div>
            <!-- end filter --->

            <!--- Start All Courses -->
            <div class="all-courses">
                @foreach ($months as $month => $items)

                    <div class="courses">

                        <!--- Start courses section -->
                        <div class="row">
                            @foreach ($items as $item)
                                @if($item->course->type=='corporate courses')
                                <span class="date"> <i class="fas fa-circle-notch"></i>{{$month}}  </span>

                                <!--- Start One course -->
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <div class="course1">
                                        <button class="add-to-cart AddToCart " data-product-id="{{$item->course->id}}" title="أضف الى العربة">
                                            <i class="fas fa-cart-plus"></i>
                                        </button>
                                        <a href="{{url('/course_single/'.$item->course->id)}}" class="cs-img">

                                            <h4 class="categ"> - البرامج التدريبية للأفراد</h4>
                                            <img src="{{url('/'.$item->course->image)}}" class="imagee">
                                        </a>
                                        <div class="cs-body">
                                            <div class="cs-title">
                                                <h3> {{{$item->course->name_ar}}} </h3>
                                                @isset($item->course->discount)
                                                    <div class="percent">{{{$item->course->discount}}} %</div>
                                                @endif                                            </div>
                                            <ul class="details">
                                                <li>
                                                    <span class="cs-icon"><i class="far fa-calendar-alt"></i></span>
                                                    <span class="start-date"> {{date('j', strtotime($item->from))}}- </span>
                                                    <span class="end-date"> {{date('j M', strtotime($item->to))}} </span>
                                                </li>
                                                <li>
                                                    <span class="cs-icon"><i class="fas fa-map-marker-alt"></i></span>
                                                    <span>{{{$item->city->name_ar}}}</span>
                                                </li>
                                                <li>
                                                    <span class="cs-icon"><i class="fas fa-coins"></i></span>
                                                    <span>
                                              @isset($item->course->discount)<span class="old-price">{{$item->course->fees}} </span>
                                                        <span class="new-price">  {{($item->course->fees)-($item->course->fees*$item->course->discount/100)}} ريال سعودى</span>
                                                        @else
                                                            <span class="new-price">  {{$item->course->fees}} ريال سعودى </span>
                                                        @endif
                                             </span>                                                 </li>
                                                <li>
                                                    <span class="cs-icon"><i class="fas fa-globe"></i></span>
                                                    <span>{{{$item->course->language}}}</span>
                                                </li>
                                            </ul>
                                            <a href="apply.html" class="btn-3">سجل الان</a>
                                        </div>
                                    </div>
                                </div>
                                <!--- End One course -->
                                @endif
                            @endforeach

                        </div>

                    </div>
            @endforeach

            <!--- End courses section -->

            </div>
            <!--- End All Courses -->

        </div>
    </section>
    <!---------------------- End Calender ---------------------->


@endsection