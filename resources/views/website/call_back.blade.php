@extends('layouts.master')
@section('content')

    <main id="main">
        <header class="heading-banner text-white bgCover" >
            <div class="container holder">
                <div class="align">
                </div>
            </div>
        </header>
        <!-- breadcrumb nav -->
        <!-- two columns -->
        <div id="two-columns" class="container">
            <div class="row">
                <!-- content -->
                <article id="content" class="col-xs-12 col-md-9">
                    <!-- content h1 -->
                    {!!Form::open( ['route' => 'website.callback.store' ,'class'=>'reviesSubmissionForm', 'method' => 'Post','files' => true]) !!}

                    <h2 class="text-noCase">Call  Me Back</h2>
                    <p> Everyone in Bltc is ready to help you. That is one of our commitments.
                        please complete the sections below and we will contact with you.
                    </p>
                    <section class="widget widget_box widget_course_select">

                        <div class="form-group">
                            <label for="name" class="formLabel fw-normal font-lato no-shrink">First Name <span class="required">*</span></label>
                            <input type="text" name="first_name" id="name" class="form-control element-block">
                        </div>
                        <div class="form-group">
                            <label for="name" class="formLabel fw-normal font-lato no-shrink">Last Name <span class="required">*</span></label>
                            <input type="text" name="last_name" id="name" class="form-control element-block">
                        </div>
                        <div class="form-group">
                            <label for="Email" class="formLabel fw-normal font-lato no-shrink">Phone <span class="required">*</span></label>
                            <input type="number" name="phone" id="Email" class="form-control element-block">
                        </div>
                        <div class="form-group">
                            <label for="text" class="formLabel fw-normal font-lato no-shrink">Company <span class="required">*</span></label>
                            <input type="text" name="company" id="text" class="form-control element-block">
                        </div>
                        <button type="submit" class="btn btn-theme btn-warning text-uppercase font-lato fw-bold">Submit</button>
                    </section>
                    </form>
                </article>
                <!-- sidebar -->
                <aside class="col-xs-12 col-md-3" id="sidebar">
                    <!-- widget course select -->
                    <section class="widget widget_categories">
                        <h3>Course Categories</h3>
                        <ul class="list-unstyled text-capitalize font-lato">
                            @foreach($categories as $cat)
                                <li class="cat-item cat-item-1"><a href="{{url('/all_in_category/'.$cat->id)}}">{{$cat->name}}</a></li>
                            @endforeach
                        </ul>
                    </section>                    <!-- widget categories -->
                    <!-- widget intro -->
                    <!-- widget popular posts -->
                    <!-- widget tags -->
                </aside>
            </div>
        </div>
    </main>

@endsection