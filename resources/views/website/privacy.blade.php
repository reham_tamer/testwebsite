@extends('layouts.master')
@section('content')
    <main id="main">
        <!-- heading banner -->
        <header class="heading-banner text-white bgCover" >
            <div class="container holder">
                <div class="align">
                </div>
            </div>
        </header>
        <!-- breadcrumb nav -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="active">Privacy policy </li>
                </ol>
            </div>
        </nav>
        <!-- text info block -->
        <article class="container text-info-block">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <h2>Privacy policy </h2>
                    <ul>
                        <li>
                          <h3>  BLTC is committed to protecting its client’s personal information that shared with us to guarantee a positive and safe experience on our website.
                          </h3> </li>
                        <li><h3>
                            All credit/debit card details and any personal information will NOT be shared, sold, stored, rented, or leased to any third parties.
                            </h3></li>
                        <li><h3>
                            The BLTC Privacy Policy applies equally to all individuals visiting www.bltc.com.sa and using the Services by Business Leadership Training Company
                            </h3></li>
                        <li>
                            <h3>  Information you provide us</h3>
                            <ul>
                                <li>
                                    We collect the information you provide to us directly. For example, we collect information when you register for a course or interact with our call center and team. The types of information we may collect includes, but not limited to:

                                </li>
                                <li>
                                    Data that you provide by filling in forms on the website.  This includes data provided at the time of registering for a course. We may also ask you for personal data (such as when you report a problem with the website). If You contact us, in writing, by email or other electronic means, we may keep a record of that correspondence; and details of your visits to the website including, but not limited to, traffic data, location data.

                                </li>
                            </ul>

                        </li>
                        <li><h3>
                                Usage and Sharing of Information Provided by You
                            </h3>
                        <ul>
                            <li>
                                BLTC does not share any personal information you provided to third parties, we may use the collected information as follows:
                            </li>
                            <li>
                                To respond to your inquiries, comments, questions or requests.
                            </li>
                            <li>
                                To monitor and statistically analyze usage of our Services and to improve the Services offered.
                            </li>
                            <li>
                                To contact you about our Services if consented by you.
                            </li>
                            <li>
                                To verify and monitor compliance with our Privacy Policy.
                            </li>
                            <li>
                                BLTC  may disclose your information in good faith if required to do so by the legal authorities that have the right to request such information.
                            </li>
                        </ul>

                        </li>
                        <li>
                            <h3>
                                Privacy policy changes

                            </h3>
                            <ul>
                                <li>
                                    The website policies and terms & conditions may be changed or updated occasionally in order to meet the requirements and standards, so it is recommended to visit these sections in order to be updated with any changes

                                </li>
                                <li>
                                    Any modifications will be effective on the day they are posted
                                </li>
                            </ul>

                        </li>
                    </ul>

                </div>


            </div>
        </article>
        <!-- counter aside -->
        <!-- why lms block -->
        <!-- aside note block -->
        <!-- professionals block -->
    </main>



@endsection