@extends('layouts.master')
@section('title')
    الجدول الزمني
@endsection
@section('content')
    <main id="main">
        <!-- heading banner -->
        <header class="heading-banner text-white bgCover" >
            <div class="container holder">
                <div class="align">
                </div>
            </div>
        </header>
        <!-- breadcrumb nav -->
        <!-- two columns -->
        <div id="two-columns" class="container">
            <div class="row">
                <!-- content -->
                <article id="content" class="col-xs-12 col-md-9">
                    <!-- content h1 -->
                    <h1 class="content-h1 fw-semi">{{$course->name}}</h1>
                    <!-- view header -->
                    <header class="view-header row">
                        <div class="col-xs-12 col-sm-9 d-flex">
                            <div class="d-col">
                                <!-- post author -->
                                <div class="post-author">
                                    <div class="alignleft no-shrink icn-wrap">
                                        <i class="far fa-bookmark"></i>
                                    </div>
                                    <div class="description-wrap">
                                        <h2 class="author-heading"><a href="#">Category</a></h2>
                                        <h3 class="author-heading-subtitle text-uppercase">{{$course->categories->name}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="aligncenter content-aligncenter">
                        <img src="{{url('/../'.$course->image)}}" style="width:828px; height: 390px; " alt="image description">
                    </div>

                    <ul class="nav nav-pills">

                        @isset($course->course_objective)
                            <li class="active"><a data-toggle="pill" href="#tab1">Course Objective</a></li>
                        @endif
                        @isset($course->why_attend)
                            <li><a data-toggle="pill" href="#tab2">Why attend </a></li>
                        @endif
                        @isset($course->course_methodology)
                            <li><a data-toggle="pill" href="#tab3">Course methodology </a></li>
                        @endif
                        @isset($course->course_outline)
                            <li><a data-toggle="pill" href="#tab4">Course outline </a></li>
                        @endif
                        @isset($course->target_audience)
                            <li><a data-toggle="pill" href="#tab5">Target audience </a></li>
                        @endif
                        @if(isset($cities[0]->from))
                            <li><a data-toggle="pill" href="#tab6">Available Dates </a></li>
                        @endif

                    </ul>

                    <div class="tab-content">
                        <div id="tab1" class="tab-pane fade in active">

                            <p>{{$course->course_objective}}</p>

                        </div>

                        <div id="tab2" class="tab-pane fade">
                            <p>{{$course->why_attend }}</p>

                        </div>

                        <div id="tab3" class="tab-pane fade">

                            <p>{{$course->course_methodology }}</p>

                        </div>

                        <div id="tab4" class="tab-pane fade">
                            <p>{{$course->course_outline }}</p>

                        </div>

                        <div id="tab5" class="tab-pane fade">
                            {{$course->target_audience }}

                        </div>

                        <div id="tab6" class="tab-pane fade">
                            <div class="panel-group sectionRowPanelGroup" id="accordion2" role="tablist" aria-multiselectable="true">
                                <!-- panel -->
                                @foreach($cities as $city)

                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading2One">
                                            <h3 class="panel-title fw-normal">
                                                <a class="accOpener"   aria-expanded="false" aria-controls="collapse2One">
												<span class="accOpenerCol">
													<i class="fas fa-chevron-circle-right accOpenerIcn"></i>{{$city->from}} <span class="label label-warning text-white text-uppercase"> TO </span>  <i>{{$city->to}}</i>
												</span>
                                                    <span class="accOpenerCol hd-phone">
													<span class="tagText bg-primary fw-semi text-white text-uppercase">{{$city->city->name}}</span>
												</span>
                                                </a>
                                            </h3>
                                        </div>

                                    </div>
                                @endforeach
                            </div>

                        </div>

                    </div>




                @if(isset($cities[0]->from))
                    <!-- bookmarkFoot -->
                        <div class="bookmarkFoot">
                            <div class="bookmarkCol">
                                <a href="{{url('/register_single_course/'.$course->id)}}" class="btn btn-theme btn-warning text-uppercase fw-bold">Register this course</a>

                            </div>
                        </div>

                    @endif

                </article>
                <!-- sidebar -->
                <aside class="col-xs-12 col-md-3" id="sidebar">
                    <!-- widget course select -->
                    <section class="widget widget_box widget_course_select">
                        <header class="widgetHead text-center bg-theme">
                            <h3 class="text-uppercase">Take This Course</h3>
                        </header>
                        @isset($course->fees)
                            <strong class="price element-block font-lato" data-label="price:"> £{{$course->fees }}</strong>
                        @endif
                        <ul class="list-unstyled font-lato">
                            @isset($course->days_num)

                                <li><i class="far fa-clock icn no-shrink"></i> Duration :{{$course->days_num }} days </li>
                            @endif
                            @isset($course->language)

                                <li><i class="far fa-address-card icn no-shrink"></i> Language : {{$course->language }} </li>
                            @endif
                        </ul>
                    </section>
                    <!-- widget categories -->
                    <section class="widget widget_categories">
                        <h3>Course Categories</h3>
                        <ul class="list-unstyled text-capitalize font-lato">
                            @foreach($categories as $cat)
                                <li class="cat-item cat-item-1"><a href="{{url('/all_in_category/'.$cat->id)}}">{{$cat->name}}</a></li>
                            @endforeach
                        </ul>
                    </section>
                    <!-- widget intro -->
                    <!-- widget popular posts -->
                    <!-- widget tags -->

                </aside>
            </div>
        </div>
    </main>


@endsection