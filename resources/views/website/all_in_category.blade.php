@extends('layouts.master')
@section('title')
    الدورات التدريبية المتاحة
@endsection
@section('content')

    <!----- Start Breadcrumbs ----->
    <section class="breadcrumbs">
        <b>الدورات التدريبية المتاحة</b>
    </section>
    <!----- End Breadcrumbs ----->





    <!---------------------- Start Calender ---------------------->
    <section class="calender mini-box">
        <div class="container">
            <!--- Start All Courses -->
            {{--<div class="all-courses">--}}

            <div class="courses">

                <!--- Start courses section -->
                <div class="row">
                @foreach ($course as $item)

                    <!--- Start One course -->
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="course1">
                                <a href="{{url('/course_details/'.$item->id)}}" class="cs-img">
                                    <img src="{{url('/'.$item->image)}}" class="imagee">
                                </a>
                                <div class="cs-body">
                                    <div class="cs-title">
                                        <h3> {{{$item->name_ar}}} </h3>
                                    </div>
                                    <ul class="details">

                                        <li>
                                            <span class="cs-icon"><i class="fas fa-globe"></i></span>
                                            <span>{{{$item->language_ar}}}</span>
                                        </li>
                                        <li>
                                            <span class="cs-icon"><i class="fas fa-calendar-alt"></i></span>
                                            <span>{{{$item->days_num}}} يوم</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--- End One course -->
                    @endforeach

                </div>

            </div>
        {{--@endforeach--}}

        <!--- End courses section -->

        {{--</div>--}}
        <!--- End All Courses -->

        </div>
    </section>
    <!---------------------- End Calender ---------------------->


@endsection