@extends('layouts.master')
@section('title')
    الجدول الزمني
@endsection
@section('content')


    <!----- Start Course Titles ----->
    <section class="course-title">
        <div class="title-img"> <img src="{{url('public/website/img/analytics.png')}}"> </div>
        <div class="title-dtls">
            <h3> {{$course->name_ar}} </h3>
            {{--<h4> تخصص {{$course->categories->name_ar}} </h4>--}}
            {{--<p class="red"> {{($course->type)== 'public courses' ? 'البرامج التدريبية للأفراد' :--}}
                                                           {{--(($course->type)== 'corporate courses' ? 'البرامج التدريبية للشركات':(--}}
                                                           {{--(($course->type)== 'E.learning' ? 'برامج التدريب الالكتروني'  :--}}
                                                           {{--'برامج نهاية الاسبوع المخفضة')))}} </p>--}}
        </div>
    </section>
    <!----- End Course Titles ----->

    <!---------------------- Start course-pdf ---------------------->
    <section class="calender cs-pdf">
        <div class="container">
            <!---- start filter -->
            <div class="filter in-filter">
                @isset($course->course_objective_ar)
                    <a class="active" href="#goals"> أهداف الدورة </a>
                @endif
                @isset($course->why_attend_ar)
                    <a href="#why_attend"> نظرة عامة </a>
                @endif
                @isset($course->course_methodology_ar)
                    <a href="#course_methodology"> منهجية التدريب </a>
                @endif
                @isset($course->course_outline_ar)
                    <a href="#course_outline">المحتوى </a>
                @endif
                @isset($course->target_audience_ar)
                    <a href="#target_audience">الفئات المستهدفة </a>
                @endif
                @isset($course->credit_id)
                    <a href="#credits"> الإعتمادات الدولية </a>
                @endif

            </div>
            <!-- end filter --->

            <!-- Start Course all details -->
            <div class="course-in" id="printed">

                <div class="sec1 sec-bg" id="goals">
                    <h3 class="red">أهداف الدورة</h3>

                    <div class="goals-in">
                        {!!html_entity_decode($course->course_objective_ar)!!}
                        <div class="goal-img">
                            <img src="{{url('public/website/img/goal.png')}}">
                        </div>
                    </div>
                </div>
                @isset($course->why_attend_ar)

                    <div class="sec1" id="why_attend">
                        <h3 class="red"> نظرة عامة </h3>

                        {!!html_entity_decode($course->why_attend_ar )!!}
                    </div>
                @endif
                @isset($course->course_methodology_ar)
                    <div class="sec1" id="course_methodology">
                        <h3 class="red"> منهجية التدريب </h3>
                        <ul class="goals row-ul">
                            {!!html_entity_decode($course->course_methodology_ar)!!}

                        </ul>
                    </div>

                @endif
                @isset($course->course_outline_ar)
                    <div class="sec1" id="course_outline">
                        <h3 class="red"> المحتوى </h3>
                        <ul class="goals row-ul">
                            {!!html_entity_decode($course->course_outline_ar)!!}
                        </ul>
                    </div>

                @endif
                @isset($course->target_audience_ar)
                    <div class="sec1" id="target_audience">
                        <h3 class="red"> الفئات المستهدفة </h3>
                        <ul class="goals row-ul">
                            {!!html_entity_decode($course->target_audience_ar)!!}

                        </ul>
                    </div>
                @endif
                @isset($course->credit_id)

                    <div class="sec1 sec-bg" id="credits">
                        <h3 class="red"> الإعتمادات الدولية</h3>

                        <div class="goals-in">
                            <div class="goals">
                                <p>
                                {!!html_entity_decode($course->credits->name_ar)!!}
                            </div>
                            <div class="goal-img nasba">
                                <img src="{{url('/'.$course->credits->image)}}">
                            </div>
                        </div>
                    </div>

                @endif
            </div>
            <div id="editor"></div>

            <!-- End Course all details -->




        </div>
    </section>
    <!---------------------- End course-pdf ---------------------->



@endsection