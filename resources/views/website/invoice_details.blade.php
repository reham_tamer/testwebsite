@extends('layouts.master')
@section('content')

    <section class="breadcrumbs">
        <b> عرض تفاصيل الفاتورة </b>
    </section>
    <!----- End Breadcrumbs ----->

    <!---------------------- Start Grid Show ---------------------->
    <section class="grids">
        <div class="container">

            <div class="course1 grid">

            </div>

        </div>
    </section>
    <!---------------------- End Grid Show ---------------------->

    <!----- Start call details ----->
    <!----- End call details ----->


    <!---------------------- Start Apply ---------------------->
    <section class="apply-cs all-sections">
        <div class="container">

            <h3 class="h3-title"> تسجيل البيانات الشخصية </h3>

            {!!Form::open( ['route' => 'website.carts.store' ,
            'class'=>'apply', 'method' => 'Post','files' => true]) !!}

            <div class="row">

                <div class="col-lg-6 col-xs-12">
                    <div class="regs-right">
                        <b class="r-title"> تفاصيل الفاتورة </b>
                        {!!Form::open( ['route' => 'website.carts.store' ,
                         'class'=>'apply', 'method' => 'Post','files' => true]) !!}
                            {{--<input type="hidden" name="course_id" value="{{$course->id}}">--}}
                            {{--<input type="hidden" name="city_date_id" value="{{$cities[0]->id}}">--}}
                            {{--<input type="hidden" name="category_id" value="{{$course->category_id}}">--}}

                        <div class="col-xs-12">
                            <div class="form-data">
                                <label> الاسم كما تريد أن يظهر فى الشهادة (عربى)</label>
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" placeholder="الاسم">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        {{--<div class="col-xs-12">--}}
                            {{--<div class="form-data">--}}
                                {{--<label> الاسم كما تريد أن يظهر فى الشهادة (انجليزى)</label>--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" name="user_name" class="form-control" placeholder="الاسم">--}}
                                    {{--<span class="focus-border"><i></i></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        <div class="col-xs-12">
                            <div class="form-data">
                                <label> جهة العمل (اختيارى) </label>
                                <div class="form-group">
                                    <input type="text" name="company" class="form-control" placeholder="مقر العمل">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <div class="form-data">
                                <label> الجوال </label>
                                <div class="form-group">
                                    <input type="number" name="phone" class="form-control" placeholder="الهاتف">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-xs-12">
                            <div class="form-data">
                                <label> البريد الإلكترونى </label>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="البريد الإلكترونى">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-6 col-xs-12">
                            <div class="form-data">
                                <label> المهنة </label>
                                <div class="form-group">
                                    <input type="text"  name="job" class="form-control" placeholder="المهنة">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>




                    </div>

                </div>

                <div class="col-lg-6 col-xs-12">
                    <div class="regs-left">
                        <b class="r-title">طلبك </b>
                        <table class="orders">
                            <tr>
                                <th>البرنامج
                                </th>
                                <th>الإجمالى</th>
                            </tr>
                            <tr> <?php
                                $count=0;
                                ?>
                                @foreach($items as $item)

                                <td>{{$item->courses->name_ar}}
                                    <b class="num-orders">x <span>{{$item->amount}}</span> </b>
                                </td>
                                <td>{{$item->amount * $item->price}} ر.س</td>
                                        <?php
                                        $count =$count+($item->amount * $item->price);
                                        ?>
                            </tr>

@endforeach
                            <tr>
                                <th>المجموع</th>
                                <td>{{$count}} ر.س</td>
                                <input type="hidden" name="total" value="{{$count}}">

                            </tr>

                        </table>

                        {{--<div class="col-xs-12 no-padding">--}}
                            {{--<div class="for-edit">--}}

                                {{--<div class="col-sm-8 col-xs-12 no-padding">--}}
                                    {{--<div class="form-data">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<input type="text" name="user_name" class="form-control" placeholder="اذا كان لديك كوبون ، اضفط هنا لاستخدامه">--}}
                                            {{--<span class="focus-border"><i></i></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                                {{--<div class="col-sm-4 col-xs-12 no-padding">--}}
                                    {{--<button type="button" class="edit-nw">--}}
                                        {{--<i class="fas fa-edit"></i>--}}
                                        {{--تعديل الطلب--}}
                                    {{--</button>--}}
                                {{--</div>--}}

                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="radio-list">
                                <label class="rad"> الدفع عبر التحويل البنكى
                                    <input type="radio" checked="checked" name="radio" value="0">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="rad"> الدفع الإلكترونى
                                    <input type="radio" name="radio" value="1">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <button type="submit" class="btn-3"> تأكيد الطلب </button>
                        </div>
                    </div>
        </form>
                </div>


    </section>

@endsection
