@extends('layouts.master')
@section('content')

    <section class="breadcrumbs">
        <b>المدربين</b>
    </section>
    <!----- End Breadcrumbs ----->




    <!---------------------- Start Trainers ---------------------->
    <section class="calender all-sections">
        <div class="container">


            <div class="row">
            @if(isset($trainers))
                @foreach($trainers as $info)
                    <!--- Start One Trainer -->
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="course1 trainer">
                                <a href="course-details.html" class="cs-img">
                                    <img src="{{url('/'.$info->image)}}">
                                </a>
                                <div class="cs-body">
                                    <div class="cs-title">
                                        <a href="#"> <h3> {{$info->name_ar}} </h3> </a>
                                    </div>
                                    <p>
                                        {{$info->body_ar}}
                                    </p>
                                    <ul class="social foot-social">
                                        <li>
                                            <a href="{{$info->link}} " class="linked">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--- End One Trainer -->
                    @endforeach
                @endif
            </div>

        </div>
    </section>
    <!---------------------- End Trainers ---------------------->

@endsection

<!----- Start Breadcrumbs ----->


