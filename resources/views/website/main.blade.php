@extends('layouts.master')
@section('content')
    <section class="header">
        <div class="container">
            <div class="row">

                <div class="col-md-6 col-sm-7 col-xs-12">
                    <div class="head-txt">
                        <h1> خبرات العالم بين يديك ..</h1>
                        <p> حلول تدريبية مختلفة للأفراد والمنظمات </p>
                        <a href="{{url('/all_courses/')}}" class="btn-3"> تصفح الدورات التدريبية </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-5 col-xs-12">
                    <a href="{{url('public/website/img/vector.png')}}" data-fancybox="header" class="head-img">
                        <img src="{{url('public/website/img/vector.png')}}">
                    </a>
                </div>

            </div>
        </div>
    </section>

    {{--<div class="slide">--}}
        {{--<div id="owl-demo" class="owl-carousel owl-theme">--}}
{{--@foreach($slider as $item)--}}
            {{--<div class="item">--}}
                {{--<img src="{{url('/'.$item->image)}}">--}}
                {{--<div class="header-overlay"></div>--}}
                {{--<div class="carousel-caption">--}}
                    {{--<p class="lead wow slideInDown">--}}
                        {{--{{$item->title_ar}}--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
{{--@endforeach--}}

        {{--</div>--}}


    {{--</div>--}}
    {{--<!--End Carousel-->--}}


    <!---------------------- Start Calender ---------------------->
    <section class="calender all-sections">
        <div class="container">

            <h3 class="h3-title red"> دوراتنا التدريبية القادمة </h3>

            <div class="row">
            @foreach ($months as $item)

                <!--- Start One course -->
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="course1">
                        {{--{!! Form::open([--}}
                    {{--'url' => '/pay',--}}
                    {{--'method' => 'POST',--}}
                    {{--'class' => 'form-horizontal'--}}
                  {{--]) !!}--}}
                            <button class="add-to-cart AddToCart " data-product-id="{{$item->course->id}}" title="أضف الى العربة">
                                <i class="fas fa-cart-plus"></i>
                            </button>
                        {{--{!!Form::close() !!}--}}

                        <a href="{{url('/course_single/'.$item->course->id)}}" class="cs-img">
                            <h4 class="categ"> - {{($item->course->type)== 'public courses' ? 'البرامج التدريبية للأفراد' :
                                                           (($item->course->type)== 'corporate courses' ? 'البرامج التدريبية للشركات':(
                                                           (($item->course->type)== 'E.learning' ? 'برامج التدريب الالكتروني'  :
                                                           'برامج نهاية الاسبوع المخفضة')))
                                    }}</h4>                            <img src="{{url('/'.$item->course->image)}}" class="imagee">
                        </a>
                        <div class="cs-body">
                            <div class="cs-title">
                                <h3> {{{$item->course->name_ar}}} </h3>
                                @isset($item->course->discount)
                                    <div class="percent">{{{$item->course->discount}}} %</div>
                                @endif
                            </div>
                            <ul class="details">
                                <li>
                                    <span class="cs-icon"><i class="far fa-calendar-alt"></i></span>
                                    <span class="start-date"> {{date('j', strtotime($item->from))}} -</span>
                                    <span class="end-date"> {{date('j M', strtotime($item->to))}} </span>
                                </li>
                                <li>
                                    <span class="cs-icon"><i class="fas fa-map-marker-alt"></i></span>
                                    <span>{{{$item->city->name_ar}}}</span>
                                </li>
                                <li>
                                    <span class="cs-icon"><i class="fas fa-coins"></i></span>
                                    @isset($item->course->discount)<span class="old-price">{{$item->course->fees}} </span>
                                    <span class="new-price">  {{($item->course->fees)-($item->course->fees*$item->course->discount/100)}} ريال سعودى</span>
                                    @else
                                        <span class="new-price">  {{$item->course->fees}} ريال سعودى </span>
                                    @endif
                                </li>
                                <li>
                                    <span class="cs-icon"><i class="fas fa-globe"></i></span>
                                    <span>{{{$item->course->language}}}</span>
                                </li>
                            </ul>
                            <a href="{{url('/register_single_course/'.$item->course->id)}}" class="btn-3">سجل الان</a>
                        </div>
                    </div>
                </div>
                <!--- End One course -->
            @endforeach

                <div class="col-xs-12">
                    <a href="{{url('/all_courses/')}}" class="btn-3 dark btn-lg"> عرض جميع الدورات </a>
                </div>

            </div>


        </div>
    </section>
    <!---------------------- End Calender ---------------------->


    <!---- New Training methods ------------>
    <section class="courses train-methods all-sections">
        <div class="container">
            <div class="all-methods">

                <h3 class="h3-title">كيف نقدم برامجنا التدريبية </h3>

                <div class="row">

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="course1 blog1 method1">
                            <div class="cs-img">
                                <img src="{{url('public/website/img/training.png')}}" class="imagee">

                            </div>
                            <div class="cs-body">
                                <div class="cs-title">
                                    <h3> التدريب الداخلى</h3>
                                </div>
                                <p>
                                     يتـم تصميـم دوراتنـا التدريبيـة الداخليـة بحيـث تكـون ملائمــة تمامــا
                                    للمنظمة ويتــم تطويــر تلــك الــدورات حتـي تتناسـب مـع أهـداف المنظمـة، التدريـب
                                    الداخـي يعتــبر حــل تدريبــي متكامــل يتــم تســليمه في مقــر المنظمـة، في الوقــت
                                    الذي يناسبك، ويعتــبر خيـار مـرن وفعــال مــن حيــث التكلفــة، ويتيــح لــك تدريــب
                                    أكــبر                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="course1 blog1 method1">
                            <div class="cs-img">
                                <img src="{{url('public/website/img/presentation.png')}}" class="imagee">
                            </div>
                            <div class="cs-body">
                                <div class="cs-title">
                                    <h3> التدريب المدمج</h3>
                                </div>
                                <p>

                                     يتــم الجمــع بــن التدريــب المتعــدد الوســائل بــن التدريــب التقليــدي
                                    في الفصــول التدريبيــة وبــن التدريـب عـبر الإنترنـت لتوفـير منهـج التدريـب المدمـج،
                                    والـذي يجمـع بـن مميـزات كلا مـن هذيـن الأسـلوبن مـن التدريـب. حيـث تعتـبر طريقـة
                                    فعالـة مـن حيـث التكلفـة وفعالـة مـن حيـث الوقـت وتوفـير أوقـات مرنـة للالتحــاق
                                    بالبرامــج التدريبية                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="course1 blog1 method1">
                            <div class="cs-img">
                                <img src="{{url('public/website/img/online-course.png')}}" class="imagee">

                            </div>
                            <div class="cs-body">
                                <div class="cs-title">
                                    <h3> التدريب الإلكترونى</h3>
                                </div>
                                <p>
                                     مــن خــلال شراكاتنــا العالميــة مــع جهــات متميــزة في
                                    مجــال التعليــم الإلكــتروني والتــي تمتلــك أعــى المعايــير التقنيــة مــع امتيازهــا
                                    بقــدر كبــير مــن التفاعليــة. ســوف توفــر لــك دورات التدريــب الإلكــتروني
                                    المرونــة والملاءمــة التــي تحتاجهــا لتطويــر حياتــك المهنيــة ومهاراتــك ومعرفتــك،
                                    حيــث تتيــح لــك الدراســة في الوقــت الملائــم وبالقــدر الــذي تريــده.
                                                                    </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="course1 blog1 method1">
                            <div class="cs-img">
                                <img src="{{url('public/website/img/report.png')}}" class="imagee">
                            </div>
                            <div class="cs-body">
                                <div class="cs-title">
                                    <h3> التدريب الإرشادى</h3>
                                </div>
                                <p>
                                    يهـدف التوجيـه الإرشـادي إلى دعمـك في ترجمـة
                                    المعرفـة والمفاهيــم النظريــة إلى تطبيقــات عمليــة ذات فائــدة عاليـة، مـن خـلال
                                    الاسـتعانة بخبـير مختـص ليسـاعدك (أو مجموعــة صغــيرة مــن الموظفــن) علــى
                                    تحســن المهــارات والأداء باســتخدام الأدوات والخــبرات العمليــة التـي يمكنـك
                                    اسـتخدامها عـى الفـور لتطويـر مسـيرتك المهنيــة والتحضــير لمواجهــة تحديــات
                                    الأعمــال.                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>

    <!-- End Training -->

    <section class="types all-sections">
        <div class="width-90">
            <h3 class="h3-title"> فئات التدريب </h3>

            <!--Start Carousel-->
            <div id="owl-types" class="owl-carousel owl-theme">
                @foreach($categories as $info)

                <div class="item">
                    <div class="type1">
                        <div class="content-in">
                            {{$info->name_ar}}
                        </div>
                    </div>
                </div>

               @endforeach

            </div>
            <!--End Carousel-->

            <a href="apply.html" class="btn-3 btn-lg"> طلب دورة تدريبية داخلية </a>
        </div>
    </section>
    <!-- End types -->

    <!-- Start Subscribe -->
    <section class="subscribe all-sections">
        <div class="container">

            <div class="sub-content">
                <h2>
                    اشترك الان فى النشرة البريدية وكن على اطلاع دائم بأحدث الدورات التدريبية والتخفيضات الخاصة
                </h2>
                <form action="#" class="apply subsc">
                    <div class="row">

                        <div class="col-sm-8 col-xs-12">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="البريد الإلكترونى">
                                <span class="focus-border"><i></i></span>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <button type="submit" class="btn-3"> اشتراك </button>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </section>
    <!-- End Subscribe -->



    <!---------------------- Start Clients ---------------------->
    <section class="clients all-sections">
        <div class="container">

            <h3 class="h3-title red"> عملائنا </h3>
            <!--Start Carousel-->
            <div id="owl-clients" class="owl-carousel owl-theme">
                @if(isset($clients))
                    @foreach($clients as $info)
                <div class="item">
                    <div class="type1">
                        <div class="content-in">
                            <img src="{{url('/'.$info->image)}}">
                        </div>
                    </div>
                </div>
                    @endforeach
                @endif

            </div>
            <!--End Carousel-->
        </div>
    </section>
    <!---------------------- End Clients ---------------------->



    <!-- Start Choose Us -->
    <section class="choose-us all-sections">
        <div class="container">

            <div class="chose-content">
                <h3 class="red"> لماذا تختارنا </h3>
                <ul class="circle-ul">
                    <li>
                        الاهتمام بالعنصر الوطنى توظيفا وتأهيا وتدريبا
                    </li>
                    <li>
                        فريق قوى مؤهل
                    </li>
                    <li>
                        الاهتمام بالعنصر الوطنى توظيفا وتأهيا وتدريبا
                    </li>
                    <li>
                        فريق قوى مؤهل
                    </li>
                    <li>
                        الاهتمام بالعنصر الوطنى توظيفا وتأهيا وتدريبا الاهتمام بالعنصر الوطنى توظيفا وتأهيا وتدريبا الاهتمام بالعنصر الوطنى توظيفا وتأهيا وتدريبا
                    </li>
                    <li>
                        فريق قوى مؤهل
                    </li>
                </ul>
            </div>

        </div>
    </section>
    <!-- End Choose Us -->



    <!---------------------- Start Partners ---------------------->
    <section class="clients all-sections">
        <div class="container">

            <h3 class="h3-title red"> شركاء النجاح </h3>
            <!--Start Carousel-->
            <div id="owl-partners" class="owl-carousel owl-theme">

                    @foreach($partners as $info)
                <div class="item">
                    <div class="type1">
                        <div class="content-in">
                            <img src="{{url('/'.$info->image)}}">
                        </div>
                    </div>
                </div>
                    @endforeach


            </div>
            <!--End Carousel-->
        </div>
    </section>
    <!---------------------- End Partners ---------------------->
@endsection