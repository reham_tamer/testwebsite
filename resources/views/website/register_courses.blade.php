@extends('layouts.master')
@section('content')

    <section class="breadcrumbs">
        <b> التسجيل فى إحدى دوراتنا التدريبية </b>
    </section>
    <!----- End Breadcrumbs ----->

    <!---------------------- Start Grid Show ---------------------->
    <section class="grids">
        <div class="container">

            <div class="course1 grid">
                <div class="cs-img">
                    <img src="{{url('/'.$course->image)}}">
                </div>
                <div class="cs-body">
                    <div class="cs-title">
                        <h3> {{$course->name_ar}} </h3>
                        @isset($course->discount)
                            <div class="percent">{{$course->discount}} %</div>
                        @endif
                    </div>
                    <ul class="details">
                        <li>
                            <span class="cs-icon"><i class="far fa-calendar-alt"></i></span>
                            <span class="start-date"> {{$cities[0]->from}}- </span>
                            <span class="end-date"> {{$cities[0]->to}} </span>
                        </li>
                        <li>
                            <span class="cs-icon"><i class="fas fa-user"></i></span>
                            <span>{{$course->instructors->name_ar}}</span>
                        </li>
                        <li>
                            <span class="cs-icon"><i class="fas fa-globe"></i></span>
                            <span>{{($course->language_ar)}}</span>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
    <!---------------------- End Grid Show ---------------------->

    <!----- Start call details ----->
    <section class="course-call">
        <div class="container">
            <div class="row">

                <div class="col-md-4 col-xs-6">
                    <div class="call1">
                        <span class="cs-icon"><i class="far fa-calendar-alt"></i></span>

                        <p>
                            مدة البرنامج <span>{{$course->days_num}} أيام </span>
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-xs-6">
                    <div class="call1">
                        <span class="cs-icon"><i class="fas fa-map-marker-alt"></i></span>
                        <p>{{$cities[0]->city->name_ar}}</p>
                    </div>
                </div>


                <div class="col-md-4 col-xs-6">
                    <div class="call1">
                        <span class="cs-icon"><i class="fas fa-coins"></i></span>
                        <p> الرسوم شامل الضريبة </p>
                        <span>
                        @isset($course->discount)<span class="old-price">{{$course->fees}} </span>
                            <span class="new-price"> {{($course->fees)-($course->fees*$course->discount/100)}} ريال سعودى</span>
                            @else
                                <span class="new-price"> {{$course->fees}} ريال سعودى </span>
                            @endif
                    </span>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!----- End call details ----->


    <!---------------------- Start Apply ---------------------->
    <section class="apply-cs all-sections">
        <div class="container">

            <h3 class="h3-title"> تسجيل البيانات الشخصية </h3>

            {!!Form::open( ['route' => 'website.courses.store' ,
            'class'=>'apply', 'method' => 'Post','files' => true]) !!}

            <div class="row">

                <div class="col-lg-6 col-xs-12">
                    <div class="regs-right">
                        <b class="r-title"> تفاصيل الفاتورة </b>
                        <div class="col-sm-6 col-xs-12">
                            <input type="hidden" name="course_id" value="{{$course->id}}">
                            <input type="hidden" name="city_date_id" value="{{$cities[0]->id}}">
                            <input type="hidden" name="category_id" value="{{$course->category_id}}">

                            <div class="form-data">
                                <label> الاسم الأول</label>
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" placeholder="الاسم">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <div class="form-data">
                                <label> الاسم الأخير</label>
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" placeholder="الاسم">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-data">
                                <label> الاسم كما تريد أن يظهر فى الشهادة (عربى)</label>
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" placeholder="الاسم">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-data">
                                <label> الاسم كما تريد أن يظهر فى الشهادة (انجليزى)</label>
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" placeholder="الاسم">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="form-data">
                                <label> جهة العمل (اختيارى) </label>
                                <div class="form-group">
                                    <input type="text" name="company" class="form-control" placeholder="مقر العمل">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xs-12">
                            <div class="form-data">
                                <label> الجوال </label>
                                <div class="form-group">
                                    <input type="number" name="phone" class="form-control" placeholder="الهاتف">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 col-xs-12">
                            <div class="form-data">
                                <label> البريد الإلكترونى </label>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="البريد الإلكترونى">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <!--

                        <div class="col-sm-6 col-xs-12">
                            <div class="form-data">
                                <label> المهنة </label>
                                <div class="form-group">
                                    <input type="text"  name="job" class="form-control" placeholder="المهنة">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>
    -->



                    </div>
                </div>

                <div class="col-lg-6 col-xs-12">
                    <div class="regs-left">
                        <b class="r-title">طلبك </b>
                        <table class="orders">
                            <tr>
                                <th>المنتج
                                </th>
                                <th>الإجمالى</th>
                            </tr>
                            <tr>
                                <th>إدارة وتشخيص الأداء
                                    <b class="num-orders">x <span>1</span> </b>
                                </th>
                                <td>2.900.00 ر.س</td>
                            </tr>
                            <tr>
                                <th>المجموع</th>
                                <td>2.900.00 ر.س</td>
                            </tr>
                            <tr>
                                <th>VAT(5%)</th>
                                <td>145.00 ر.س</td>
                            </tr>
                            <tr>
                                <th>الإجمالى</th>
                                <td>3.045.00 ر.س</td>
                            </tr>
                        </table>

                        <div class="col-xs-12 no-padding">
                            <div class="for-edit">

                                <div class="col-sm-8 col-xs-12 no-padding">
                                    <div class="form-data">
                                        <div class="form-group">
                                            <input type="text" name="user_name" class="form-control" placeholder="اذا كان لديك كوبون ، اضفط هنا لاستخدامه">
                                            <span class="focus-border"><i></i></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-4 col-xs-12 no-padding">
                                    <button type="button" class="edit-nw">
                                        <i class="fas fa-edit"></i>
                                        تعديل الطلب
                                    </button>
                                </div>

                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="radio-list">
                                <label class="rad"> الدفع عبر التحويل البنكى
                                    <input type="radio" checked="checked" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="rad"> الدفع الإلكترونى
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <button type="submit" class="btn-3"> تأكيد الطلب </button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection
