@extends('layouts.master')
@section('content')




    <!----- Start methodology ----->
    <a href="{{url('public/website/img/Training-methodology.png')}}" data-fancybox="منهجية التدريب" class="methodology">
        <img src="{{url('public/website/img/Training-methodology.png')}}">
    </a>
    <!----- End methodology ----->


    <!---------------------- Start Training Methodology ---------------------->
    <section class="apply-cs all-sections methodo">
        <div class="container">

            <h3 class="h3-title"> منهجية التدريب </h3>

            <div class="apply">
                <ul class="circle-ul ">
                    <li>
                        تقييم الاحتياجات التدريبية
                    </li>
                    <li>
                        تقديم الحلول التدريبية والتطويرية
                    </li>
                    <li>
                        تقييم الاحتياجات التدريبية
                    </li>
                    <li>
                        تقديم الحلول التدريبية والتطويرية
                    </li>
                    <li>
                        تقييم الاحتياجات التدريبية
                    </li>
                    <li>
                        تقديم الحلول التدريبية والتطويرية
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!---------------------- End Training Methodology ---------------------->



@endsection
