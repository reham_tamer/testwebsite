@extends('layouts.master')
@section('title')
المدونات@endsection
@section('content')

    <!----- Start Breadcrumbs ----->
    <section class="breadcrumbs">
        <b>مركز المعرفة</b>
    </section>
    <!----- End Breadcrumbs ----->



    <!----- Start Search ----->
    <section class="searching">
            <form action="{{url('/search_blogs')}}" class="search form-lg" method="get">


            <div class="form-width">
                <div class="form-data">
                    <div class="form-group">
                        <span class="srch1"> <i class="fas fa-search"></i> </span>
                        <input type="text" name="item" class="form-control" placeholder="ابحث عن ...">
                        <span class="focus-border"><i></i></span>
                    </div>
                </div>
            </div>

            <button type="submit" class="form-width btn-3 srch-to"> بحث <i class="fas fa-search"></i> </button>

        </form>
    </section>
    <!----- End Search ----->


    <!---------------------- Start Blog Content ---------------------->
    <section class="calender all-sections">
        <div class="container">
            <div class="row">


                <!--- Start All Blogs -->
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                    <div class="blog">
                        @foreach($blogs as $item)
                        <!--- Start One course -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="course1 blog1">
                                <a href="{{url('/blog_single/'.$item->id)}}" class="cs-img">
                                    <img src="{{url('/'.$item->image)}}" class="imagee">
                                </a>
                                <div class="cs-body">
                                    <div class="cs-title">
                                        <a href="{{url('/blog_single/'.$item->id)}}">
                                            <h3> {{{$item->title_ar}}} </h3>
                                        </a>
                                    </div>
                                    <p>
                                        {!!html_entity_decode($item->body_ar)!!}
                                    </p>
                                    <a href="{{url('/blog_single/'.$item->id)}}" class="btn-3">المزيد</a>
                                </div>
                            </div>
                        </div>
                        <!--- End One course -->

                        @endforeach

                    </div>
                </div>
                <!--- End All Blogs -->

                <!--- Start Related Blogs -->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="related">
                        <h3>مواضيع ذات علاقة</h3>
                        <a href="blog-single.html" class="sm-blog">
                            <p>
                                لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على
                            </p>
                            <h4><img class="calendar" src="img/calendar.png">15 نوفمبر 2019</h4>
                        </a>
                        <a href="blog-single.html" class="sm-blog">
                            <p>
                                لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على
                            </p>
                            <h4><img class="calendar" src="img/calendar.png">15 نوفمبر 2019</h4>
                        </a>
                        <a href="blog-single.html" class="sm-blog">
                            <p>
                                لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على
                            </p>
                            <h4><img class="calendar" src="img/calendar.png">15 نوفمبر 2019</h4>
                        </a>
                        <a href="blog-single.html" class="sm-blog">
                            <p>
                                لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على
                            </p>
                            <h4><img class="calendar" src="img/calendar.png">15 نوفمبر 2019</h4>
                        </a>
                    </div>
                </div>
                <!--- End Related Blogs -->

            </div>
        </div>
    </section>
    <!---------------------- End Blog Content ---------------------->



@endsection