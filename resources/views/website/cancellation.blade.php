@extends('layouts.master')
@section('content')
    <main id="main">
        <!-- heading banner -->
        <header class="heading-banner text-white bgCover" >
            <div class="container holder">
                <div class="align">
                </div>
            </div>
        </header>
        <!-- breadcrumb nav -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="active">Terms and conditions </li>
                </ol>
            </div>
        </nav>
        <!-- text info block -->
        <article class="container text-info-block">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <h2>                                Registration cancellation and refund
                    </h2>
                    <ul>
                        <li>
                            You must contact us no later than 3 weeks before starting the event, to cancel your registration.
                        </li>
                        <li>
                            For cancellations, your registration fee will be refunded less a 50% cancellation fee.
                        </li>
                        <li>
                            No refunds will be given for registration cancellations made after that time.
                        </li>
                        <li>
                            Cancellation requests must identify the registrant's name, email address, confirmation number, and the amount paid or a copy of registration receipt. Send cancellation requests to ……………...

                        </li>

                    </ul>

                </div>


            </div>
        </article>
        <!-- counter aside -->
        <!-- why lms block -->
        <!-- aside note block -->
        <!-- professionals block -->
    </main>



@endsection