@extends('layouts.master')
@section('content')

    <!--- Start Map -->
    <div id="map"> </div>
    <!--- End Map -->


    <!---------------------- Start Apply ---------------------->
    <section class="apply-cs all-sections contacting">
        <div class="container">


            <div class="row">
                <div class="col-md-6 col-sm-5 col-xs-12">
                    <div class="call-info">
                        <h4 class="red"> تواصل معنا </h4>
                        <ul>
                            <li>
                                <span class="deal-icon"><i class="fas fa-map-marker-alt"></i></span>
                                الطابق الثانى ، مبنى بنك الجزيرة ، طريق خرصة الرياض ، المملكة العربية السعودية
                            </li>
                            <li>
                                <span class="deal-icon"><i class="fas fa-envelope"></i></span>
                                <a href="mailto:ccc@bltc.com">ccc@bltc.com</a>
                            </li>
                            <li>
                                <span class="deal-icon"><i class="fas fa-home"></i></span>
                                <a href="www.bltc.com.sa"> www.bltc.com.sa </a>
                            </li>
                            <li>
                                <span class="deal-icon"><i class="fas fa-phone-alt"></i></span>
                                <a href="tel:+966582452">+966582452</a>
                            </li>
                            <li>
                                <span class="deal-icon"><i class="fas fa-print"></i></span>
                                <a href="tel:+966582452">+966582452</a>
                            </li>
                            <li>
                                <span class="deal-icon"><i class="fas fa-fax"></i></span>
                                ص.ب 7495 الرياض 13211
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="col-md-6 col-sm-7 col-xs-12">
                        {!!Form::open( ['route' => 'website.contact.store' ,'class'=>'apply sm-form',
                         'method' => 'Post',
                         'files' => true]) !!}

                        <h4 class="red"> شكاوى ومقترحات</h4>

                        <div class="col-xs-12 no-padding">
                            <div class="form-data">
                                <label> الاسم </label>
                                <div class="form-group">
                                    <input type="text" name="first_name" class="form-control" placeholder="الاسم">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>
                    <div class="col-xs-12 no-padding">
                        <div class="form-data">
                            <label>  الهاتف </label>
                            <div class="form-group">
                                <input type="number" name="phone" class="form-control" placeholder=" الهاتف">
                                <span class="focus-border"><i></i></span>
                            </div>
                        </div>
                    </div>

                        <div class="col-xs-12 no-padding">
                            <div class="form-data">
                                <label> البريد الإلكترونى </label>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="البريد الإلكترونى">
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 no-padding">
                            <div class="form-data">
                                <label> التفاصيل </label>
                                <div class="form-group">
                                    <textarea rows="4" cols="95" name="message" id="inbox" class="form-control input-lg" data-fv-field="inbox" placeholder="رسالتك"></textarea>
                                    <span class="focus-border"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <button type="submit" class="btn-3"> إرسال </button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </section>
    <!---------------------- End Apply ---------------------->


@endsection