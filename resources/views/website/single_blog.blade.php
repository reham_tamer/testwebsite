@extends('layouts.master')
@section('content')


    <section class="cs-instructor who-us refr">
        <div class="container">

            <div class="instructor">
                <div class="instructor-dtls">
                </div>
            </div>

        </div>
    </section>
    <!----- End Who Us ----->

    <!---------------------- Start Refrence Details ---------------------->
    <section class="apply-cs all-sections new-colors">
        <div class="container">

            <h3 class="h3-title"> {{$blog->title_ar}} </h3>

            <div class="apply">
                {!!html_entity_decode($blog->body_ar)!!}
            </div>

        </div>
    </section>
@endsection