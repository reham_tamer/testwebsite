@extends('layouts.master')
@section('content')
    <!---------------------- Start Apply ---------------------->
    <section class="apply-cs all-sections">
        <div class="container">


            <div class="apply">
                <div class=" confirm">

                    <img src="{{url('public/website/img/success.png')}}">

                    <h3 class="green">
                        تم التسجيل فى الدورة التدريبية بنجاح
                    </h3>

                    <p>
شكرا لاختيارك BLTC                    </p>

                    <a href="http://www.example.com/index.html" class="btn-3 blue" download="How-to-download-file.pdf">تحميل الفاتورة</a>

                </div>
            </div>

        </div>
    </section>
    <!---------------------- End Apply ---------------------->
@endsection