@extends('layouts.master')
@section('title')
    عربة الشراء@endsection
@section('content')


    <!----- Start Breadcrumbs ----->
    <section class="breadcrumbs">
        <b> سلة الشراء </b>
    </section>
    <!----- End Breadcrumbs ----->






    <!---------------------- Start Apply ---------------------->
    <section class="apply-cs all-sections cart-section">
        <div class="container">

            <h3 class="h3-title"> عربة الشراء </h3>

            <div class="carts">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="border-bottom titles">
                            <div class="col-xs-5 text-center">
                                <p class="tb-title">الدورة التدريبية</p>
                            </div>
                            <div class="col-xs-2 text-center">
                                <p class="tb-title">السعر</p>
                            </div>
                            <div class="col-xs-3 text-center">
                                <p class="tb-title">عدد المقاعد</p>
                            </div>
                            <div class="col-xs-2 text-center">
                                <p class="tb-title">الاجمالى</p>
                            </div>
                        </div>
                    </div>
                    @isset($items)
                        @foreach($items as $cartItem)

                    <div class="col-xs-12">
                        <div class="border-bottom talab1 item" id="removable{{$cartItem->id}}">
                            <div class="col-xs-5">
                                <div class="tlb-right">
                                    {{--<button class="cancel">--}}
                                        {{--<i class="fa fa-times"></i>--}}
                                    {{--</button>--}}
                                    <div>
                                    <button  id="{{$cartItem->id}}" data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('website.carts.destroy',$cartItem->id)}}"
                                             type="button" class="destroy btn btn-danger ">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                    </div>
                                    <div class="talab-img">
                                        <a data-fancybox="Shopping Cart" href="{{url('/'.$cartItem->courses->image)}}">
                                            <img class="scaleimg imagee" src="{{url('/'.$cartItem->courses->image)}}">
                                        </a>
                                    </div>
                                    <div class="small-b">{{$cartItem->courses->name_ar}}</div>
                                </div>
                            </div>
                            <div class="col-xs-2 text-center">
                                <div class="talab-dtls1">
                                    <b>{{$cartItem->price}} ر.س</b>
                                </div>
                            </div>

                            <div class="col-xs-3 text-center">
                                <div class="talab-dtls1">

                                    <form class="quant">
                                        <div class="count">
                                            <div class="value-button allac2 add" id="increase"  data-product-id="{{$cartItem->courses->id}}"  value="Increase Value">+</div>
                                            <div class="value-button allac2 minus" id="decrease"  data-product-id="{{$cartItem->courses->id}}" value="Decrease Value">-</div>
                                            <input type="number" id="number2" class="number qty" value="{{$cartItem->amount}}" data-task="{{$cartItem->courses->fees}}">
                                        </div>
                                    </form>

                                </div>
                            </div>

                            <div class="col-xs-2 text-center">
                                <div class="talab-dtls1">
                                    <p class="prices total">
                                        <span  class="totalPrice">
                                            {{$cartItem->price * $cartItem->amount}}
                                        </span>
                                        <span class="ryal">ر.س</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                        @endforeach


                    <div class="col-xs-12">
                        <a href="{{url('/invoice-details/')}}" class="square-an f-left">
                            متابعة للدفع
                            <i class="fa fa-arrow-left"></i>
                        </a>
                    </div>

                    @endif


                </div>
            </div>

        </div>
    </section>
    <!---------------------- End Apply ---------------------->


@endsection

