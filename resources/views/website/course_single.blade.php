@extends('layouts.master')
@section('title')
    الجدول الزمني
@endsection
@section('content')


    <!----- Start Course Titles ----->
    <section class="course-title">
        <div class="title-img"> <img src="{{url('public/website/img/analytics.png')}}"> </div>
        <div class="title-dtls">
            <h3> {{$course->name_ar}} </h3>
            {{--<h4> تخصص {{$course->categories->name_ar}} </h4>--}}
            {{--<p class="red"> {{($course->type)== 'public courses' ? 'البرامج التدريبية للأفراد' :--}}
                                                           {{--(($course->type)== 'corporate courses' ? 'البرامج التدريبية للشركات':(--}}
                                                           {{--(($course->type)== 'E.learning' ? 'برامج التدريب الالكتروني'  :--}}
                                                           {{--'برامج نهاية الاسبوع المخفضة')))}} </p>--}}
        </div>
    </section>
    <!----- End Course Titles ----->

    <!----- Start Course Instructor ----->
    <section class="cs-instructor">
        <div class="container">

            <div class="instructor">
                <div class="instructor-img"> <img src="{{url('/'.$course->instructors->image)}}"> </div>
                <div class="instructor-dtls">
                    <h3>{{$course->instructors->name_ar}} </h3>
                    <h3> {{$course->instructors->body_ar}} </h3>
                    <ul class="social foot-social">
                        <li>
                            <a href="{{$course->instructors->link}}">
                                <i class="fab fa-linkedin"></i>
                            </a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<i class="fab fa-twitter"></i>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<i class="fab fa-instagram"></i>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    </ul>

                    <div class="anchors">
                        <a href="{{url('/register_single_course/'.$course->id)}}" class="btn-3 blue">سجل الان</a>
                        <a href="request-course.html" class="btn-3 dark">طلب دورة تدريبية داخلية</a>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!----- End Course Instructor ----->




    <!----- Start call details ----->
    <section class="course-call">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-xs-6">
                    <div class="call1">
                        <span class="cs-icon"><i class="far fa-calendar-alt"></i></span>
                        <p>
                            من <span class="start-date"> {{$cities[0]->from}}  </span>
                            إلى <span class="end-date"> {{$cities[0]->to}} </span>
                        </p>
                        <p>
                            مدة البرنامج <span>{{$course->days_num}} أيام </span>
                        </p>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="call1">
                        <span class="cs-icon"><i class="fas fa-history"></i></span>
                        <p>
                            من <span class="start-date"> 04:00 عصرا </span>
                            إلى <span class="end-date"> 09:00 مساء</span>
                        </p>
                    </div>
                </div>


                <div class="col-md-3 col-xs-6">
                    <div class="call1">
                        <span class="cs-icon"><i class="fas fa-map-marker-alt"></i></span>
                        <p>{{$cities[0]->city->name_ar}}</p>
                    </div>
                </div>


                <div class="col-md-3 col-xs-6">
                    <div class="call1">
                        <span class="cs-icon"><i class="fas fa-coins"></i></span>
                        <p> الرسوم شامل الضريبة </p>
                        <p>{{$course->fees}} ريال سعودى</p>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!----- End call details ----->
    <!---------------------- Start course-pdf ---------------------->
    <section class="calender cs-pdf">
        <div class="container">
            <!---- start filter -->
            <div class="filter in-filter">
                @isset($course->course_objective)
                <a class="active" href="#goals"> أهداف الدورة </a>
                @endif
                @isset($course->why_attend_ar)
                <a href="#why_attend"> نظرة عامة </a>
                @endif
                @isset($course->course_methodology_ar)
                    <a href="#course_methodology"> منهجية التدريب </a>
                @endif
                @isset($course->course_outline_ar)
                    <a href="#course_outline">المحتوى </a>
                @endif
                @isset($course->target_audience_ar)
                        <a href="#target_audience">الفئات المستهدفة </a>
                @endif
                    @isset($course->credit_id)
                <a href="#credits"> الإعتمادات الدولية </a>
                    @endif

            </div>
            <!-- end filter --->

            <!-- Start Course all details -->
            <div class="course-in" id="printed">

                <div class="sec1 sec-bg" id="goals">
                    <h3 class="red">أهداف الدورة</h3>

                    <div class="goals-in">
                        {!!html_entity_decode($course->course_objective_ar)!!}
                         <div class="goal-img">
                             <img src="{{url('public/website/img/goal.png')}}">
                         </div>
                     </div>
                 </div>
                 @isset($course->why_attend_ar)

                 <div class="sec1" id="why_attend">
                     <h3 class="red"> نظرة عامة </h3>

                     {!!html_entity_decode($course->why_attend_ar )!!}
                </div>
                @endif
                @isset($course->course_methodology_ar)
                <div class="sec1" id="course_methodology">
                    <h3 class="red"> منهجية التدريب </h3>
                    <ul class="goals row-ul">
                        {!!html_entity_decode($course->course_methodology_ar)!!}

                </ul>
                </div>

                @endif
                @isset($course->course_outline_ar)
                <div class="sec1" id="course_outline">
                    <h3 class="red"> المحتوى </h3>
                    <ul class="goals row-ul">
                    {!!html_entity_decode($course->course_outline_ar)!!}
                </ul>
                </div>

                @endif
                @isset($course->target_audience_ar)
                <div class="sec1" id="target_audience">
                <h3 class="red"> الفئات المستهدفة </h3>
                <ul class="goals row-ul">
                    {!!html_entity_decode($course->target_audience_ar)!!}

                </ul>
                </div>
                @endif
                @isset($course->credit_id)

                <div class="sec1 sec-bg" id="credits">
                <h3 class="red"> الإعتمادات الدولية</h3>

                <div class="goals-in">
                <div class="goals">
                    <p>
                    {!!html_entity_decode($course->credits->name_ar)!!}
                </div>
                <div class="goal-img nasba">
                    <img src="{{url('/'.$course->credits->image)}}">
                </div>
                </div>
                </div>

                @endif
                </div>
                <div id="editor"></div>

                <!-- End Course all details -->

                <div class="anchors">
                <button class="btn-3 blue" onclick='printDiv();'>تحميل ملف الدورة</button>
                <a href="request-course.html" class="btn-3 dark">ارسال إيصال التحويل البنكى</a>
                </div>


                </div>
                </section>
                <!---------------------- End course-pdf ---------------------->



@endsection