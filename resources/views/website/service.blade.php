@extends('layouts.master')
@section('content')

    <section class="abouting references">
        <div class="container">

            <div class="row">
@foreach($service as $item)
                <div class="col-sm-6 col-xs-12">
                        <a href="{{url('/subservices/'.$item->id)}}" class="view">

                        <h3>
                            <img src="{{url('/'.$item->image)}}">
                            {{$item->name}}
                        </h3>
                        {!!html_entity_decode($item->body)!!}
                        </a>
                </div>

@endforeach
                {{--<div class="col-xs-12">--}}
                    {{--<a href="http://www.example.com/index.html" class="btn-3 blue" download="How-to-download-file.pdf">تحميل برشور خدمات الاستشارات</a href="http://www.example.com/index.html">--}}
                {{--</div>--}}

            </div>


        </div>
    </section>
@endsection