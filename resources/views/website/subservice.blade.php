@extends('layouts.master')
@section('content')

    <!----- Start Who Us ----->
    <section class="cs-instructor who-us refr">
        <div class="container">

            <div class="instructor">
                <div class="instructor-dtls">
                </div>
            </div>

        </div>
    </section>
    <!----- End Who Us ----->

    <!---------------------- Start Refrence Details ---------------------->
    <section class="apply-cs all-sections new-colors">
        <div class="container">

            <h3 class="h3-title"> {{$service->name_ar}} </h3>

            <div class="all-p apply">
                {!!html_entity_decode($service->body_ar)!!}
<!--
                <ul class="goals">
                    <li> this is a very good center </li>
                    <li> this is a very good center </li>
                    <li> this is a very good center </li>
                    <li> this is a very good center </li>
                    <li> this is a very good center </li>
                </ul>
-->
            </div>

        </div>
    </section>
    <!---------------------- End Refrence Details ---------------------->


@endsection