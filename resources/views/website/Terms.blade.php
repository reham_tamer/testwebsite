@extends('layouts.master')
@section('content')
        <!-- text info block -->
        <article class="container text-info-block">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <h2>Terms and conditions </h2>
                    <ul>
                        <li><h3>
                                We accept payments online using Visa and MasterCard
                                credit/debit card in SAR

                            </h3></li>
                        <li><h3>
                                Registration questions should be sent to …………………..

                            </h3></li>
                        <li><h3>
                                Registration payment policy

                            </h3>
                        <ul>
                            <li>
                                The full payment registration fee must be made within 48 hours of registration
                            </li>
                            <li>
                                If payments are not received during this period the registration will be canceled
                            </li>
                            <li>
                                Successful Registrants will receive an immediate email notification with the invoice.
                            </li>
                            <li>
                                The registration fee is determined at the time of payment.

                            </li>
                            <li>
                                It’s recommended to  Save and/or scan the receipt for registration purposes.

                            </li>
                        </ul>
                        </li>
                        <li><h3>
                                Registration confirmation

                            </h3>
                            <ul>
                                <li>
                                    A confirmation email will be sent with 5 working days when confirming the receipt of payment.                                </li>
                                <li>
                                    If you did not get a confirmation email within 5 working days please contact us on ……………………..                                </li>
                            </ul>
                        </li>

                        <li><h3>
                                Registration cancellation and refund

                            </h3>
                            <ul>
                                <li>
                                    You must contact us no later than 3 weeks before starting the event, to cancel your registration.
                                </li>
                                <li>
                                    For cancellations, your registration fee will be refunded less a 50% cancellation fee.
                                </li>
                                <li>
                                    No refunds will be given for registration cancellations made after that time.
                                </li>
                                <li>
                                    Cancellation requests must identify the registrant's name, email address, confirmation number, and the amount paid or a copy of registration receipt. Send cancellation requests to ……………...

                                </li>

                            </ul>
                        </li>
                    </ul>


                </div>


            </div>
        </article>
        <!-- counter aside -->
        <!-- why lms block -->
        <!-- aside note block -->
        <!-- professionals block -->



@endsection