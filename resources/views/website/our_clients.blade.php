@extends('layouts.master')
@section('content')

    <section class="breadcrumbs img-crumb">
        <b>
            <img src="{{url('public/website/img/teamwork.png')}}">
عملاؤنا
        </b>
    </section>
    <!----- End Breadcrumbs ----->


    <!----- Start partners ----->
    <section class=" partners">
        <div class="container">


            <div class="blocks">
                <div class="row">
                    @if(isset($clients))
                        @foreach($clients as $info)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="view">
                                    <img src="{{url('/'.$info->image)}}">
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>


        </div>
    </section>
    <!----- End partners ----->




@endsection