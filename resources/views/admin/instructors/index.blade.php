@extends('admin.layout')
@section('title')
    Instructors
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
            Show all Instructors
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Instructor Name </th>
                    <th>Instructor image </th>
                    <th>linked in account</th>
                    <th>Edit</th>
                    <th>Delete</th>

                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($instructor as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center">{{$info->name_ar}}</td>
                        <td><img class="img-responsive center-block"
                                 style="width:70px;height: 70px"
                                 src="{{url('/../'.$info->image)}}"/>
                        </td>
                        <td class="center">{{$info->link}}</td>

                        <td class="center"><a class="btn btn-success"
                                              href="{{URL::route('admin.instructors.edit',$info->id)}}">
                                <i class="fa fa-edit "></i>
                            </a>
                        </td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.instructors.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection