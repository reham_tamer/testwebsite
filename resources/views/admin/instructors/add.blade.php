@extends('admin.layout')
@section('title') add new instructor
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new instructor
        </div>
        <div class="box-content">
            {!! Form::open([
               'route' => 'admin.instructors.store',
               'method' => 'POST',
               'class' => 'form-horizontal',
               'files' => true
             ]) !!}
            @include('admin.instructors.form')
            {!!Form::close() !!}

        </div>
    </div>

@endsection

