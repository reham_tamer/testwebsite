@extends('admin.layout')
@section('title') edit instructor
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Edit instructor data
        </div>
        <div class="box-content">
            {!! Form::model($instructor,
                          ['route'=>['admin.instructors.update',$instructor->id],
                           'method'=>'PATCH',
                           'files' => true
            ])!!}
            @include('admin.instructors.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

