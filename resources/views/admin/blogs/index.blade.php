@extends('admin.layout')
@section('title')
    Blogs
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
           Show all Blogs
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Blog title(english) </th>
                    <th>Blog title (arabic)</th>
                    <th>date</th>
                    <th>Blog image</th>
                    <th>Edit</th>
                    <th>Delete</th>


                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($blogs as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center">{{$info->title_en}}</td>
                        <td class="center">{{$info->title_ar}}</td>
                        <td class="center">{{$info->date}}</td>
                        <td><img class="img-responsive center-block"
                                 style="width:70px;height: 70px"
                                 src="{{url('/'.$info->image)}}"/>
                        </td>
                        <td class="center"><a class="btn btn-success"
                                              href="{{URL::route('admin.blogs.edit',$info->id)}}">
                                <i class="fa fa-edit "></i>
                            </a>
                        </td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.blogs.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    @endsection