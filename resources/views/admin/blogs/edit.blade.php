@extends('admin.layout')
@section('title') update
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
Update blog        </div>
        <div class="box-content">
            {!! Form::model($blogs,
                          ['route'=>['admin.blogs.update',$services->id],
                           'method'=>'PATCH'])!!}
            @include('admin.blogs.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

