@extends('admin.layout')
@section('title') add new blog
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

            <div class="box">
                <div class="box-header">
                Add new blog
                </div>
                <div class="box-content">
                    {!! Form::open([
                       'route' => 'admin.blogs.store',
                       'method' => 'POST',
                       'class' => 'form-horizontal'
                     ]) !!}
                    @include('admin.blogs.form')
                        {!!Form::close() !!}

                    </div>
                </div>

@endsection

