@extends('admin.layout')
@section('title') edit service
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Edit partner service
        </div>
        <div class="box-content">
            {!! Form::model($services,
                          ['route'=>['admin.services.update',$services->id],
                           'method'=>'PATCH',
                           'files' => true
            ])!!}
            @include('admin.services.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

