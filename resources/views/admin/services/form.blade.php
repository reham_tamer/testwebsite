@inject('service','App\Models\MainService')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
        <div class="col-lg-12">
            {!! Form::label('Service Name') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('name',null,['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    <div class="col-lg-12">
        {!! Form::label('Service Name (arabic)') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::text('name_ar',null,['class'=>'form-control']) !!}
        @if ($errors->has('name_ar'))
            <span class="help-block">
                    <strong>{{ $errors->first('name_ar') }}</strong>
                </span>
        @endif
    </div>


    <div class="col-lg-12">
        {!! Form::label('Service description') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::textArea('body',null,['class'=>'form-control  summernote',
            'id' =>'summernote',
             'rows' => 10]) !!}
        @if ($errors->has('body'))
            <span class="help-block">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
        @endif'
    </div>
    <div class="col-lg-12">
        {!! Form::label('Service description (arabic)') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::textArea('body_ar',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10]) !!}
        @if ($errors->has('body_ar'))
            <span class="help-block">
                    <strong>{{ $errors->first('body_ar') }}</strong>
                </span>
        @endif
    </div>

    <div class="col-lg-12">
        {!! Form::label('Main Service Name') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::select('main_service_id',$service->pluck('en_name','id')->toArray(),null,[
                           'class'=>'form-control',
                           'placeholder'=>'--please select main service --'
                           ]) !!}
        @if ($errors->has('main_service_id'))
            <span class="help-block">
                    <strong>{{ $errors->first('main_service_id') }}</strong>
                </span>
        @endif
    </div>



    <div class="col-lg-12">
        {!! Form::label('Service Image') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::file('image' , [
           "class" => "form-control file_upload_preview",
           "data-preview-file-type" => "text"
       ]) !!}
        @if ($errors->has('image'))
            <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
        @endif
    </div>
    <div class="col-md-12" style="text-align: center;">
        <!-- Button trigger modal -->
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</div>