@extends('admin.layout')
@section('title')
    Services
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
            Show all partners
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Service Name </th>
                    <th>Main Service Name </th>
                    <th>Service description </th>
                    <th>Service logo </th>
                    <th>Edit</th>
                    <th>Delete</th>

                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($services as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center">{{$info->name}}</td>
                        <td class="center">{{$info->service->en_name}}</td>
                        <td class="center">{{$info->body}}</td>
                        <td><img class="img-responsive center-block"
                                 style="width:70px;height: 70px"
                                 src="{{url('/../'.$info->image)}}"/>
                        </td>
                        <td class="center"><a class="btn btn-success"
                                              href="{{URL::route('admin.services.edit',$info->id)}}">
                                <i class="fa fa-edit "></i>
                            </a>
                        </td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.services.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection