@extends('admin.layout')
@section('title') add new service
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new service
        </div>
        <div class="box-content">
            {!! Form::open([
               'route' => 'admin.services.store',
               'method' => 'POST',
               'class' => 'form-horizontal',
               'files' => true
             ]) !!}
            @include('admin.services.form')
            {!!Form::close() !!}

        </div>
    </div>

@endsection

