@extends('admin.layout')
@section('title')
    Cities
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
           Show all cities
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>City Name </th>
                    <th>City Name (arabic)</th>
                    <th>Edit</th>
                    <th>Delete</th>


                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($cities as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center">{{$info->name}}</td>
                        <td class="center">{{$info->name_ar}}</td>

                        <td class="center"><a class="btn btn-success"
                                              href="{{URL::route('admin.city.edit',$info->id)}}">
                                <i class="fa fa-edit "></i>
                            </a>
                        </td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.city.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    @endsection