
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="form-group">

    <div class="col-lg-12">
        {!! Form::label('City Name(english)') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::text('name',null,['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
        {!! Form::label('City Name in (Arabic)') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::text('name_ar',null,['class'=>'form-control']) !!}
        @if ($errors->has('name_ar'))
            <span class="help-block">
                    <strong>{{ $errors->first('name_ar') }}</strong>
                </span>
        @endif
    </div>
    </div>
    <div class="form-group">

    <div class="col-md-12" style="text-align: center;">
        <!-- Button trigger modal -->
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    </div></div>