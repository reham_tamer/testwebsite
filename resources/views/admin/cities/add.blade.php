@extends('admin.layout')
@section('title') add new city
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

            <div class="box">
                <div class="box-header">
                Add new city
                </div>
                <div class="box-content">
                    {!! Form::open([
                       'route' => 'admin.city.store',
                       'method' => 'POST',
                       'class' => 'form-horizontal'
                     ]) !!}
                    @include('admin.cities.form')
                        {!!Form::close() !!}

                    </div>
                </div>

@endsection

