<!-- Main sidebar -->
<div class="sidebar sidebar-main ">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left">
                        <img src="" class="img-circle img-sm" alt="">
                    </a>

                    <div class="media-body">
                        <span class="media-heading text-semibold">{{Auth::user()->user_name}}</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-pin text-size-small"></i>  Admin
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">


                <ul class="navigation navigation-main navigation-accordion">


                    <!-- Main -->
                    <li class="navigation-header"><i class="icon-menu"
                                                     title="Main pages"></i><span>Main Setting </span> </li>
                    <li class="{{(Request::is('dashboard') ? 'active' : '')}}"><a href="{{asset('dashboard')}}"><i
                    class="icon-home4"></i> <span> home page</span></a>
                    </li>



                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->
