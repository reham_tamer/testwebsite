
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
        <div class="col-lg-12">
            {!! Form::label('Credit Name (english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('name_en',null,['class'=>'form-control']) !!}
            @if ($errors->has('name_en'))
                <span class="help-block">
                    <strong>{{ $errors->first('name_en') }}</strong>
                </span>
            @endif
        </div>
    <div class="col-lg-12">
        {!! Form::label('Credit description (english)') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::textArea('body_en',null,['class'=>'form-control']) !!}
        @if ($errors->has('body_en'))
            <span class="help-block">
                    <strong>{{ $errors->first('body_en') }}</strong>
                </span>
        @endif

    </div>
        <div class="col-lg-12">
            {!! Form::label('Credit Name (arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('name_ar',null,['class'=>'form-control']) !!}
            @if ($errors->has('name_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('name_ar') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-lg-12">
            {!! Form::label('Credit description (arabic)') !!}
        </div>

        <div class="col-lg-12">
            {!! Form::textArea('body_ar',null,['class'=>'form-control']) !!}
            @if ($errors->has('body_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('body_ar') }}</strong>
                </span>
            @endif
        </div>

    <div class="col-lg-12">
        {!! Form::label('Credit Image') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::file('image' , [
           "class" => "form-control file_upload_preview",
           "data-preview-file-type" => "text"
       ]) !!}
        @if ($errors->has('image'))
            <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
        @endif
    </div>
    <div class="col-md-12" style="text-align: center;">
        <!-- Button trigger modal -->
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</div>