@extends('admin.layout')
@section('title') edit credit
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Edit credit data
        </div>
        <div class="box-content">
            {!! Form::model($instructor,
                          ['route'=>['admin.credits.update',$instructor->id],
                           'method'=>'PATCH',
                           'files' => true
            ])!!}
            @include('admin.credits.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

