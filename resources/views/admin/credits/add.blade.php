@extends('admin.layout')
@section('title') add new credit
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new credit
        </div>
        <div class="box-content">
            {!! Form::open([
               'route' => 'admin.credits.store',
               'method' => 'POST',
               'class' => 'form-horizontal',
               'files' => true
             ]) !!}
            @include('admin.credits.form')
            {!!Form::close() !!}

        </div>
    </div>

@endsection

