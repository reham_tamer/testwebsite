@extends('admin.layout')
@section('title') edite city
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new course
        </div>
        <div class="box-content">

            {!! Form::model($city,
                         ['route'=>['admin.coursecities.update',$city->id],
                          'method'=>'PATCH',
                          'files' => true
           ])!!}
            @inject('city','App\Models\City')
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    {!! Form::label('City') !!}
                </div>
                <div class="col-lg-12">
                    {!! Form::select('city_id',$city->pluck('name','id')->toArray(),null,[
                                       'class'=>'form-control',
                                       'placeholder'=>'--please select course city--'
                                       ]) !!}
                    @if ($errors->has('city_id'))
                        <span class="help-block">
                    <strong>{{ $errors->first('city_id') }}</strong>
                </span>
                    @endif
                </div>

                <div class="col-lg-12">
                    {!! Form::label('Date From') !!}
                </div>
                <div class="col-lg-12">
                    {!! Form::date('from',null,['class'=>'form-control']) !!}
                    @if ($errors->has('from'))
                        <span class="help-block">
                    <strong>{{ $errors->first('from') }}</strong>
                </span>
                    @endif
                </div>
                <div class="col-lg-12">
                    {!! Form::label('Date To') !!}
                </div>
                <div class="col-lg-12">
                    {!! Form::date('to',null,['class'=>'form-control']) !!}
                    @if ($errors->has('to'))
                        <span class="help-block">
                    <strong>{{ $errors->first('to') }}</strong>
                </span>
                    @endif
                </div>
                <br>
                <div class="col-md-12" style="text-align: center;">
                    <!-- Button trigger modal -->
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            {!!Form::close() !!}

        </div>
    </div>

@endsection

