@extends('admin.layout')
@section('title')
    Course cities
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
            Show all cities
        </div>
        <br>
<div class="box-title">
    <a class="btn btn-success" href="{{url('dashboard/coursecities/create/'.$course_id)}}">
         Add New City
    </a>
</div>
        <br>
        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>City Name </th>
                    <th>Course From</th>
                    <th>Course To</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($cities as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center">{{$info->city->name}}</td>
                        <td class="center">{{$info->from}}</td>
                        <td class="center">{{$info->to}}</td>

                        <td class="center"><a class="btn btn-success"
                                              href="{{URL::route('admin.coursecities.edit',$info->id)}}">
                                <i class="fa fa-edit "></i>
                            </a>
                        </td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.coursecities.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection