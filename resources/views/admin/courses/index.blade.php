@extends('admin.layout')
@section('title')
    Courses
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
            Show all Courses
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Course Name </th>
                    <th>Category </th>
                    <th>Course Duration </th>
                    <th>Language </th>
                    <th>Fees </th>
                    <th>Image </th>
                    <th>Status </th>
                    <th>Cities </th>
                    <th>Edit</th>
                    <th>Delete</th>


                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($courses as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center">{{$info->name}}</td>
                        <td class="center">{{$info->categories->name}}</td>
                        <td class="center">{{$info->days_num}}</td>
                        <td class="center">{{$info->language}}</td>
                        <td class="center">{{$info->fees}}</td>
                        <td><img class="img-responsive center-block"
                                             style="width:70px;height: 70px"
                                             src="{{url('/../'.$info->image)}}"/>
                        </td>   
                        <td>
                        @if($info->status== '1')
                            <a href="{{url('dashboard/course/'.$info->id.'/un-approve')}}"
                               class="btn btn-xs btn-danger"><i class="fa fa-times">activate</i>  </a>
                        @else
                            <a href="{{url('dashboard/course/'.$info->id.'/approve')}}"
                               class="btn btn-xs btn-success"><i class="fa fa-check">deactivate</i> </a>
                        @endif
                    </td>
                        <td>
                                <a href="{{url('dashboard/coursecities/'.$info->id.'/all')}}"
                                   class="btn btn-xs btn-warning">Cities</a>
                        </td>
                        <!--<td class="center">-->
                        <!--    <a class="btn btn-info"-->
                        <!--                      href="{{URL::route('admin.course.show',$info->id)}}">-->
                        <!--        <i class="fa fa-info "></i>-->
                        <!--    </a>-->
                        <!--</td>-->
                        <td class="center"><a class="btn btn-success"
                                              href="{{URL::route('admin.course.edit',$info->id)}}">
                                <i class="fa fa-edit "></i>
                            </a>
                        </td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.course.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection