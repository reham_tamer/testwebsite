@extends('admin.layout')
@section('title') add new course
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new course
        </div>
        <div class="box-content">
            {!! Form::open([
               'route' => 'admin.course.store',
               'method' => 'POST',
               'class' => 'form-horizontal',
               'files' => true
             ]) !!}
            @include('admin.courses.form')
            {!!Form::close() !!}

        </div>
    </div>

@endsection

