@inject('cat','App\Models\Category')
@inject('inst','App\Models\Instructor')
@inject('credits','App\Models\Credit')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Course Name(english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('name',null,['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Course Name(arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('name_ar',null,['class'=>'form-control']) !!}
            @if ($errors->has('name_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('name_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Category Name') !!}
        </div>
        <div class="col-lg-12">
             {!! Form::select('category_id',$cat->pluck('name','id')->toArray(),null,[
                                'class'=>'form-control',
                                'placeholder'=>'--please select course category--'
                                ]) !!}
            @if ($errors->has('category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Number of Days') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::number('days_num',null,['class'=>'form-control']) !!}
            @if ($errors->has('days_num'))
                <span class="help-block">
                    <strong>{{ $errors->first('days_num') }}</strong>
                </span>
            @endif
        </div>

    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Language(english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('language',null,['class'=>'form-control']) !!}
            @if ($errors->has('language'))
                <span class="help-block">
                    <strong>{{ $errors->first('language') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Language(arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('language_ar',null,['class'=>'form-control']) !!}
            @if ($errors->has('language_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('language_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Why Attend (english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('why_attend',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10
                  ]) !!}
            @if ($errors->has('why_attend'))
                <span class="help-block">
                    <strong>{{ $errors->first('why_attend') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Why Attend (arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('why_attend_ar',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10]) !!}
            @if ($errors->has('why_attend_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('why_attend_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Course Methodology (english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('course_methodology',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10 ]) !!}
            @if ($errors->has('course_methodology'))
                <span class="help-block">
                    <strong>{{ $errors->first('course_methodology') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Course Methodology (arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('methodology_ar',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10 ]) !!}
            @if ($errors->has('methodology_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('methodology_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Course Objective (english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('course_objective',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10 ]) !!}
            @if ($errors->has('course_objective'))
                <span class="help-block">
                    <strong>{{ $errors->first('course_objective') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Course Objective (arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('course_objective_ar',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10 ]) !!}
            @if ($errors->has('course_objective_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('course_objective_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Target Audience (english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('target_audience',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10 ]) !!}
            @if ($errors->has('target_audience'))
                <span class="help-block">
                    <strong>{{ $errors->first('target_audience') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Target Audience (arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('target_audience_ar',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10 ]) !!}
            @if ($errors->has('target_audience_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('target_audience_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Fees') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::number('fees',null,['class'=>'form-control']) !!}
            @if ($errors->has('fees'))
                <span class="help-block">
                    <strong>{{ $errors->first('fees') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Course Outline') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('course_outline',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10 ]) !!}
            @if ($errors->has('course_outline'))
                <span class="help-block">
                    <strong>{{ $errors->first('course_outline') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Course Outline (arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('course_outline_ar',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10
             ]) !!}
            @if ($errors->has('course_outline_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('course_outline_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Course Image') !!}
        </div>
        <div class="col-lg-12">
             {!! Form::file('image' , [
               "class" => "form-control file_upload_preview",
                "data-preview-file-type" => "text"
              ]) !!}  
               @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
            {!! Form::label('Course Discount Percentage') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::number('discount',null,['class'=>'form-control']) !!}
            @if ($errors->has('discount'))
                <span class="help-block">
                    <strong>{{ $errors->first('discount') }}</strong>
                </span>
            @endif
        </div></div>

    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Instructor') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::select('instructor_id',$inst->pluck('name_en','id')->toArray(),null,[
                               'class'=>'form-control',
                               'placeholder'=>'--please select course instructor--'
                               ]) !!}
            @if ($errors->has('instructor_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('instructor_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-lg-12">
        {!! Form::label('International Credits') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::select('credit_id',$credits->pluck('name_en','id')->toArray(),null,[
                           'class'=>'form-control',
                           'placeholder'=>'--please select International Credit--'
                           ]) !!}
        @if ($errors->has('credit_id'))
            <span class="help-block">
                    <strong>{{ $errors->first('credit_id') }}</strong>
                </span>
        @endif
    </div>
</div>



<div class="form-group col-md-12 ">
        <br>
        <br>

        <label>Calender Type :</label>
        <br>

        {!! Form::label('type','public courses') !!}
        {!! Form::radio('type','0') !!}
        <br>

        {!! Form::label('type','corporate courses') !!}
        {!! Form::radio('type','1') !!}
<br>
        {!! Form::label('type','E. learning') !!}
        {!! Form::radio('type','2') !!}
        <br>

        {!! Form::label('type','weekend crash courses') !!}
        {!! Form::radio('type','3') !!}

    </div>


    <div class="form-group">

    <div class="col-md-12" style="text-align: center;">
        <!-- Button trigger modal -->
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</div>
</div>
























