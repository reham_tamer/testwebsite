@extends('admin.layout')
@section('title') add new main service
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new main service
        </div>
        <div class="box-content">
            {!! Form::model($services,
                          ['route'=>['admin.mainservices.update',$services->id],
                           'method'=>'PATCH'])!!}
            @include('admin.main_services.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

