
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="form-group">

    <div class="col-lg-12">
        {!! Form::label('Main Service Name(english)') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::text('en_name',null,['class'=>'form-control']) !!}
        @if ($errors->has('en_name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('en_name') }}</strong>
                                    </span>
        @endif
    </div>
    </div>
    <div class="form-group">

    <div class="col-lg-12">
        {!! Form::label('Main Service Name in (Arabic)') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::text('ar_name',null,['class'=>'form-control']) !!}
        @if ($errors->has('ar_name'))
            <span class="help-block">
                    <strong>{{ $errors->first('ar_name') }}</strong>
                </span>
        @endif
    </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Description (english)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('body_en',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10
                  ]) !!}
            @if ($errors->has('body_en'))
                <span class="help-block">
                    <strong>{{ $errors->first('body_en') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Description  (arabic)') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::textarea('body_ar',null,['class'=>'form-control summernote',
            'id' =>'summernote',
             'rows' => 10
                  ]) !!}
            @if ($errors->has('body_ar'))
                <span class="help-block">
                    <strong>{{ $errors->first('body_ar') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">

        <div class="col-lg-12">
            {!! Form::label('Service Image') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::file('image' , [
              "class" => "form-control file_upload_preview",
               "data-preview-file-type" => "text"
             ]) !!}
            @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif
        </div>
    </div>



    <div class="form-group">

    <div class="col-md-12" style="text-align: center;">
        <!-- Button trigger modal -->
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    </div></div>