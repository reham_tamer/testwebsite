@extends('admin.layout')
@section('title') add new main service
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

            <div class="box">
                <div class="box-header">
                Add new main service
                </div>
                <div class="box-content">
                    {!! Form::open([
                       'route' => 'admin.mainservices.store',
                       'method' => 'POST',
                       'class' => 'form-horizontal'
                     ]) !!}
                    @include('admin.main_services.form')
                        {!!Form::close() !!}

                    </div>
                </div>

@endsection

