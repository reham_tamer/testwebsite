@extends('admin.layout')
@section('title') edit category
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Edit client data
        </div>
        <div class="box-content">
            {!! Form::model($client,
                          ['route'=>['admin.clients.update',$client->id],
                           'method'=>'PATCH',
                           'files' => true
            ])!!}
            @include('admin.clients.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

