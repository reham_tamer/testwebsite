@extends('admin.layout')
@section('title') add new client
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new client
        </div>
        <div class="box-content">
            {!! Form::open([
               'route' => 'admin.clients.store',
               'method' => 'POST',
               'class' => 'form-horizontal',
               'files' => true
             ]) !!}
            @include('admin.clients.form')
            {!!Form::close() !!}

        </div>
    </div>

@endsection

