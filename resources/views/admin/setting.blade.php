@extends('admin.layout')
@section('title')
    {{$settings_page}}
@endsection

@section('content')
    <!-- Vertical form options -->
    <div class="row">
        <div class="col-md-12">
            {!!Form::open( ['route' => 'admin.settings.store' , 'method' => 'Post','files'=>true]) !!}
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">التحكم ب {{$settings_page}}</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">

                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    @foreach($settings as $setting)
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            @if($setting->type == 1)
                                <label for="exampleInputEmail1">{{$setting->title}}</label>
                                {!! Form::text($setting->name.'[]',$setting->ar_value,['class'=>'form-control'])!!}
                                <br>
                                {!! Form::text($setting->name.'[]',$setting->en_value,['class'=>'form-control'])!!}
                            @elseif($setting->type == 3)
                                <label for="exampleInputEmail1">{{$setting->title}}</label>
                                {!! Form::textarea($setting->name.'[]',$setting->ar_value,['class'=>'form-control'])!!}
                                <br>
                                {!! Form::textarea($setting->name.'[]',$setting->en_value,['class'=>'form-control'])!!}
                            @elseif($setting->type == 4)
                                <label for="exampleInputEmail1">{{$setting->title}}</label>
                                {!! Form::number($setting->name[],$setting->ar_value,['class'=>'form-control'])!!}
                                {!! Form::number($setting->name[],$setting->en_value,['class'=>'form-control'])!!}
                            @elseif($setting->type == 5)
                                <label for="exampleInputEmail1">{{$setting->title}}</label>
                                {!! Form::url($setting->name.'[]',$setting->value(),['class'=>'form-control'])!!}
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        <br>
                    @endforeach
                    <div class="text-right">
                        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
                    </div>
                    {!!Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="/admin/assets/js/pages/form_layouts.js"></script>
@endsection