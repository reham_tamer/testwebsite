@extends('admin.layout')
@section('header')
    @endsection
@section('title')
Main Page @endsection

@section('content')

    <div class="row">
        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                <i class="fa fa-list  red"></i>
                <span class="title">Courses</span>
                <span class="value">{{count(App\Models\Course::all())}}</span>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                <i class="fa fa-phone yellow"></i>
                <span class="title">Course Requests</span>
                <span class="value">{{count(App\Models\RequestCourse::all())}}</span>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                <i class="fa fa-th-large blue"></i>
                <span class="title">Categories</span>
                <span class="value">{{count(App\Models\Category::all())}}</span>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                    <i class="fa fa-phone green"></i>
                    <span class="title">Call Me Back</span>
                <span class="value">{{count(App\Models\CallMeBack::all())}}</span>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                <i class="fa fa-envelope red"></i>
                <span class="title">Contact Us</span>
                <span class="value">{{count(App\Models\ContactUs::all())}}</span>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                <i class="fa fa-beer yellow"></i>
                <span class="title">Services</span>
                <span class="value">{{count(App\Models\Service::all())}}</span>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                <i class="fa fa-users blue"></i>
                <span class="title">Clients</span>
                <span class="value">{{count(App\Models\Client::all())}}</span>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
            <div class="smallstat box">
                <i class="fa fa-map-marker green"></i>
                <span class="title">Cities</span>
                <span class="value">{{count(App\Models\City::all())}}</span>
            </div>
        </div>

    </div>
@endsection

    @section('script')
        <!-- Theme JS files -->
            <script type="text/javascript" src="/admin/assets/js/plugins/notifications/jgrowl.min.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/ui/moment/moment.min.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/daterangepicker.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/anytime.min.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/picker.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/legacy.js"></script>
            <script type="text/javascript" src="{{asset('admin/assets/jquery-locationpicker-plugin-master/dist/locationpicker.jquery.js')}}"></script>

            <script type="text/javascript" src="/admin/assets/js/core/app.js"></script>
            <script type="text/javascript" src="/admin/assets/js/pages/picker_date.js"></script>
            <script type="text/javascript" src="{{asset('admin/assets/js/pages/form_layouts.js')}}"></script>
@endsection