<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <title>BLTC DashBoard</title>
    <meta name="description" content="SimpliQ - Flat & Responsive Bootstrap Admin Template.">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="SimpliQ, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <!-- end: Meta -->

    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->

    <!-- start: CSS -->
    <link href="{{asset('public/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/assets/css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/admin/assets/css/retina.min.css')}}" rel="stylesheet">
    <!-- end: CSS -->


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>

    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset('admin/assets/js/respond.min.js')}}"></script>
    <script src="{{asset('admin/assets/css/ie6-8.css')}}"></script>

    <![endif]-->

    <!-- start: Favicon and Touch Icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('public/admin/assets/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('public/admin/assets/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('public/admin/assets/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{asset('public/admin/assets/ico/apple-touch-icon-57-precomposed.png')}}">
    <link rel="shortcut icon" href="{{asset('public/admin/assets/ico/favicon.png')}}">
    <!-- end: Favicon and Touch Icons -->

</head>
<style>
    input[placeholder], [placeholder], [placeholder] {

        color: #252638 !important;
    }
</style>
<body >

<!-- Page container -->
<div class="container">
    <div class="row">

        <div class="row">
            <div class="login-box">
                @if(session()->has('status'))
                    <div class="alert alert-danger">
                        <center>{{session()->get('status')}}</center>
                    </div>
            @endif

            <!-- Advanced login -->
                <form method="POST" action="{{ route('login') }}">
                    {!! csrf_field() !!}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object text-warning-400 login-icon-logo">
                                <img class="img-responsive" width="150px" height="150px" src="{{asset('admin/logo.png')}}" alt="">
                            </div>
                            <h5 class="content-group-lg"> Login </h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-right">
                            <input id="email" oninvalid="this.setCustomValidity('من فضلك قم بادخال الايميل بشكل صحيح')" type="email" placeholder=" Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group has-feedback has-feedback-right">

                            <input id="password" type="password" placeholder=" Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn bg-primary btn-block" style="background-color: #28343a;border-color: #28343a"> Login <i class="icon-circle-left2 position-right"></i></button>
                        </div>
                        <span class="help-block text-center no-margin">جميع الحقوق محفوظة  <a href="http://ar.bltc.com.sa/">شركة قيادة الاعمال للتدريب </a></span>
                    </div>
                </form>
                <!-- /advanced login -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
<script src="{{asset('public/admin/assets/js/jquery-3.1.0.min.js')}}"></script>

<!--<![endif]-->

<!--[if IE]>

<script src="{{asset('admin/assets/js/jquery-1.11.0.min.js')}}}"></script>

<![endif]-->

<!--[if !IE]>-->

<script type="text/javascript">
    window.jQuery || document.write("<script src='{{asset('admin/assets/js/jquery-3.1.0.min.js')}}}'>"+"<"+"/script>");
</script>

<!--<![endif]-->

<!--[if IE]>

<script type="text/javascript">
    window.jQuery || document.write("<script src='{{asset('admin/assets/js/jquery-1.11.0.min.js')}}'>"+"<"+"/script>");
</script>

<![endif]-->
<script src="{{asset('public/admin/assets/js/jquery-migrate-1.4.1.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/bootstrap.min.js')}}"></script>




<!-- theme scripts -->
<script src="{{asset('public/admin/assets/js/custom.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/core.min.js')}}"></script>

<!-- end: JavaScript-->
</body>
</html>