@extends('admin.layout')
@section('title')
    Categories
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
            Show all categories
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Category Name </th>
                    <th>Category Name (arabic) </th>
                    <th>Edit</th>
                    <th>Delete</th>


                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($categories as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center"><a href="{{URL::route('admin.category.show',$info->id)}}">{{$info->name}}</a></td>
                        <td class="center"><a href="{{URL::route('admin.category.show',$info->id)}}">{{$info->name_ar}}</a></td>
                        <td class="center"><a class="btn btn-success"
                                              href="{{URL::route('admin.category.edit',$info->id)}}">
                                <i class="fa fa-edit "></i>
                            </a>
                        </td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.category.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection