@extends('admin.layout')
@section('title')
    show all requests
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
            Show all  requests
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>User Name </th>
                    <th>Email </th>
                    <th>Phone </th>
                    <th>Job </th>
                    <th>Company </th>
                    <th>Course name </th>
                    <th>Course Category </th>
                    <th>Request Date </th>
                    <th>City </th>

                    <th>Delete</th>

                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection