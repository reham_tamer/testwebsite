@if(Session::has('flash_message'))
    <script type="text/javascript">
        swal({
            position: 'center',
            type: 'success',
            title:'success',
            text: '{{session('flash_message')}}',
            showConfirmButton: false,
            timer: 3000
        })
        </script>
    @endif
