@extends('admin.layout')
@section('title') edit partner
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Edit partner data
        </div>
        <div class="box-content">
            {!! Form::model($partner,
                          ['route'=>['admin.partners.update',$partner->id],
                           'method'=>'PATCH',
                           'files' => true
            ])!!}
            @include('admin.partners.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

