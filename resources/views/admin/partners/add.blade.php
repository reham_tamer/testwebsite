@extends('admin.layout')
@section('title') add new partner
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new partner
        </div>
        <div class="box-content">
            {!! Form::open([
               'route' => 'admin.partners.store',
               'method' => 'POST',
               'class' => 'form-horizontal',
               'files' => true
             ]) !!}
            @include('admin.partners.form')
            {!!Form::close() !!}

        </div>
    </div>

@endsection

