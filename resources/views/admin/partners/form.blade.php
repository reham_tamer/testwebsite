
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
        <div class="col-lg-12">
            {!! Form::label('Partner Name') !!}
        </div>
        <div class="col-lg-12">
            {!! Form::text('name',null,['class'=>'form-control']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    <div class="col-lg-12">
        {!! Form::label('Partner description') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::text('body',null,['class'=>'form-control']) !!}
        @if ($errors->has('body'))
            <span class="help-block">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
        @endif
    </div>
    <div class="col-lg-12">
        {!! Form::label('Partner Image') !!}
    </div>
    <div class="col-lg-12">
        {!! Form::file('image' , [
           "class" => "form-control file_upload_preview",
           "data-preview-file-type" => "text"
       ]) !!}
        @if ($errors->has('image'))
            <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
        @endif
    </div>
    <div class="col-md-12" style="text-align: center;">
        <!-- Button trigger modal -->
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</div>