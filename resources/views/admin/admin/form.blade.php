
@if (count($errors) > 0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="form-group col-md-6 pull-left">
    <label>الاسم  </label>  <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  اسم الادمن'])!!}
</div>

<div class="form-group col-md-6 pull-left">
    <label> الإيميل </label>   <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'أكتب هنا  الايميل'])!!}
</div>

<div class="form-group col-md-6 pull-left">
    <label> الجوال </label>  <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::text("phone",null,['class'=>'form-control','placeholder'=>'أكتب هنا رقم الجوال'])!!}
</div>
<div class="form-group col-md-6 pull-left">
    <label>المدينة </label>  <span class="label bg-danger help-inline">مطلوب</span>
    <div>
        {!! Form::select("city_id",$cities,null,['class'=>'form-control','id'=>'city_id','placeholder'=>'اختر المدينه '])!!}
    </div>

</div>
<div class="form-group col-md-6 pull-left">
    <label>الحى </label>
    <div>
        {!! Form::select("region_id",[],null,['class'=>'form-control','id'=>'regions','placeholder'=>'اختر المدينة اولا '])!!}
    </div>
</div>
<div class="form-group col-md-6 pull-left">
    <label> العنوان </label>  <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::text("address",null,['class'=>'form-control','placeholder'=>'أكتب هنا العنوان'])!!}
</div>
<div class="form-group col-md-6 pull-left">
    <label>كلمة المرور </label>   <span class="label bg-danger help-inline">مطلوب</span>
    <input type="password" class="form-control" name="password" placeholder="اكتب هنا كلمة المرور">
</div>

<div class="form-group col-md-6 pull-left">
    <label>   إعادة كتابة كلمة المرور </label> <span class="label bg-danger help-inline">مطلوب</span>
    <input type="password" class="form-control" name="password_confirmation" placeholder="أعد هنا كتابة كلمة المرور">
</div>

<div class="form-group col-md-6 pull-left">
    <label>    صورة العضو  </label> <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::file("image",null,['class'=>'form-control '])!!}
    {{--{{dd($user)}}--}}
    @if (isset($user))
    <img src="{{getimg($user->image)}}" class="img-preview">
    @endif
</div>

<div class="form-group col-md-12 pull-left">

    <label>    الموقع على الخريطة  </label> <span class="label bg-danger help-inline">مطلوب</span>

    <div class="form-group">
        <div id="map" style="width: 100%; height: 300px;"></div>

        <div class="clearfix">&nbsp;</div>
        <div class="m-t-small">
            <div class="col-sm-4">
                <label class="p-r-small control-label">خط الطول</label>
            </div>
            <div class="col-sm-6">
                {{ Form::text('lat', null,['id'=>'us_restaurant-lat','class'=>'form-control']) }}
            </div>
            <div class="col-sm-4">
                <label class="p-r-small  control-label">خط العرض </label>
            </div>
            <div class="col-sm-6">
                {{ Form::text('long', null,['id'=>'us_restaurant-lon','class'=>'form-control']) }}
            </div>
        </div>
    </div>
</div>
<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>
@include('owner.map')
<script src="{{asset('admin\assets\js\plugins\pickers\location\location.js')}}"></script>
@push('scripts')
    <script>
        $("#city_id").change(function () {
            var city_id = $("#city_id").val();
            console.log(city_id);
            var base_url = "{{asset('/')}}";
            $.ajax({
                type: 'get',
                url: base_url + "/dashboard/ajax_city/" + city_id,
                success: function (data) {
                    console.log(data);
                    $.each(data, function (key, value) {
                        var option = '<option value="' + value.id + '">' + value.ar_name + '</option>';
                        $("#regions").append(option);
                    });
                },
                error: function () {

                }
            });

        });
        $('#map').locationpicker({
            location: {
                latitude: "{{isset($user) ? $user->lat : 31.037933}}",
                longitude:"{{isset($user) ? $user->long : 31.381523}}"
            },
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us_restaurant-lat'),
                longitudeInput: $('#us_restaurant-lon'),
                locationNameInput: $('#us_restaurant-address')
            },
            enableAutocomplete: false,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });
    </script>
@endpush