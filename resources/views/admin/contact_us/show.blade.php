@extends('admin.layout')
@section('title')
    show all messages
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="box">
        <div class="box-header" data-original-title>
            Show all  messages
        </div>

        <div class="box-content">
            <table id="example2" class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>First Name </th>
                    <th>Last Name </th>
                    <th>Email </th>
                    <th>Phone </th>
                    <th>Message </th>
                    <th>Company </th>
                    <th>Delete</th>

                </tr>
                </thead>
                <tbody>
                <?php $i = 0 ?>
                @foreach($call as $info)
                    <?php $i++ ?>
                    <tr id="removable{{$info->id}}">
                        <td>{{$i}}</td>
                        <td class="center">{{$info->first_name}}</td>
                        <td class="center">{{$info->last_name}}</td>
                        <td class="center">{{$info->email}}</td>
                        <td class="center">{{$info->phone}}</td>
                        <td class="center">{{$info->message}}</td>
                        <td class="center">{{$info->company}}</td>

                        <td class="center">
                            <button id="{{$info->id}}" data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('admin.contactus.destroy',$info->id)}}"
                                    type="button" class="destroy btn btn-danger "><i class="fa fa-trash-o "></i>
                            </button>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection