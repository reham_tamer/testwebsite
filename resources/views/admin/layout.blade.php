<!DOCTYPE html>
<html lang="en">
<head>

  <!-- start: Meta -->
  <meta charset="utf-8">
  <title>
    BLTC DashBoard
    @yield('title')
  </title>
  <meta name="description" content="SimpliQ - Flat & Responsive Bootstrap Admin Template.">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="SimpliQ, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <!-- end: Meta -->

  <!-- start: Mobile Specific -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- end: Mobile Specific -->

  <!-- start: CSS -->
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
{!!  Html::style('public/cust/reham.css')  !!}

  <link href="{{asset('css/reham.css')}}" rel="stylesheet">
  <link href="{{asset('public/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('public/admin/assets/css/style.min.css')}}" rel="stylesheet">
  <link href="{{asset('public/admin/assets/css/retina.min.css')}}" rel="stylesheet">
  <link href="{{asset('public/admin/assets/summernote.css')}}" rel="stylesheet">

  <link href="{{asset('public/admin/plugins/bootstrap-fileinput/css/fileinput.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('public/admin/assets/sweetalert2.min.css')}}">

  <!-- end: CSS -->


  <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>

  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <script src="{{asset('admin/assets/js/respond.min.js')}}"></script>
  <script src="{{asset('admin/assets/css/ie6-8.css')}}"></script>

  <![endif]-->

  <!-- start: Favicon and Touch Icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('public/admin/assets/ico/apple-touch-icon-144-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('public/admin/assets/ico/apple-touch-icon-114-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('public/admin/assets/ico/apple-touch-icon-72-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{asset('public/admin/assets/ico/apple-touch-icon-57-precomposed.png')}}">
  <link rel="shortcut icon" href="{{asset('public/admin/assets/ico/favicon.png')}}">
  <!-- end: Favicon and Touch Icons -->

</head>

<body>
        <!-- Start Loading-Page -->
    <div class="loader">
        <div class="loading">
            <div class="book">
                <figure class="page"></figure>
                <figure class="page"></figure>
                <figure class="page"></figure>
            </div>

            <h1>BLTC</h1>
        </div>
    </div>
    <!-- End Loading-Page -->

    
<!-- start: Header -->
<header class="navbar">
  <div class="container">
    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".sidebar-nav.nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a id="main-menu-toggle" class="hidden-xs open"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand col-lg-2 col-sm-1 col-xs-12" href="#"><span>BLTC DashBoard</span></a>
    <!-- start: Header Menu -->
    <div class="nav-no-collapse header-nav">
      <ul class="nav navbar-nav pull-right">

        <!-- start: Notifications Dropdown -->

        <!-- start: User Dropdown -->
        <li class="dropdown">
          <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
            <div class="user">
              <span class="name">Admin </span>
            </div>
          </a>
          <ul class="dropdown-menu">
            <li class="dropdown-menu-title">

            </li>
            <li><a href="#" onclick="$('#logout').submit()"><i class="icon-switch2"></i>  logout</a></li>
            {!! Form::open(['route'=>'logout','id'=>'logout','class'=>'hide']) !!}
            {!! Form::close() !!}          </ul>
        </li>
        <!-- end: User Dropdown -->
      </ul>
    </div>
    <!-- end: Header Menu -->

  </div>
</header>
<!-- end: Header -->

<div class="container">
    <div class="row">

      <!-- start: Main Menu -->
      <div id="sidebar-left" class="col-lg-2 col-sm-1">

        <div class="nav-collapse sidebar-nav collapse navbar-collapse bs-navbar-collapse">
          <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="index.html"><i class="fa fa-bar-chart-o"></i><span class="hidden-sm"> Dashboard</span></a></li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-map-marker"></i><span class="hidden-sm"> Cities</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.city.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new city</span></a></li>
                <li><a class="submenu" href="{{route('admin.city.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all cities</span></a></li>
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="ui-elements.html#"><i class="fa fa-chain"></i><span class="hidden-sm"> Categories</span> </a>
              <ul>
                <li><a class="submenu" href="{{route('admin.category.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new category</span></a></li>
                <li><a class="submenu" href="{{route('admin.category.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all categories</span></a></li>
                <!-- Profile Page - Cooming Soone
                <li><a class="submenu" href="page-profile.html"><i class="fa fa-male"></i><span class="hidden-sm"> User Profile</span></a></li>
                -->
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-edit"></i><span class="hidden-sm"> Courses</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.course.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new course</span></a></li>
                <li><a class="submenu" href="{{route('admin.course.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all courses</span></a></li>
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-users"></i><span class="hidden-sm"> Our Clients </span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.clients.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new client</span></a></li>
                <li><a class="submenu" href="{{route('admin.clients.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all clients</span></a></li>
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-user"></i><span class="hidden-sm"> Our Partners</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.partners.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new partner</span></a></li>
                <li><a class="submenu" href="{{route('admin.partners.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all partner</span></a></li>
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-beer"></i><span class="hidden-sm"> Sub Services</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.services.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new service</span></a></li>
                <li><a class="submenu" href="{{route('admin.services.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all services</span></a></li>
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-beer"></i><span class="hidden-sm"> Services</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.mainservices.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new main service</span></a></li>
                <li><a class="submenu" href="{{route('admin.mainservices.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all main services</span></a></li>
              </ul>
            </li>

            <li>
            <a class="dropmenu" href="#"><i class="fa fa-phone"></i><span class="hidden-sm"> Call me back</span></a>
            <ul>
              <li><a class="submenu" href="{{route('admin.callmeback.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">show</span></a></li>
            </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-mail-forward"></i><span class="hidden-sm"> Contact Us</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.contactus.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all messages</span></a></li>
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm">Courses Requests</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.requestcourse.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all requests</span></a></li>
              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm">Sliders</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.slider.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new image</span></a></li>

                <li><a class="submenu" href="{{route('admin.slider.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all images</span></a></li>

              </ul>
            </li>

            <li>
              <a class="dropmenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm">Instructors</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.instructors.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new instructor</span></a></li>

                <li><a class="submenu" href="{{route('admin.instructors.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all instructors</span></a></li>

              </ul>
            </li>

            <li>
              <a class="dropmenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm">international credits</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.credits.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new credit</span></a></li>

                <li><a class="submenu" href="{{route('admin.credits.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all credits</span></a></li>

              </ul>
            </li>
            <li>
              <a class="dropmenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm">Blogs</span></a>
              <ul>
                <li><a class="submenu" href="{{route('admin.blogs.create')}}"><i class="fa fa-plus"></i><span class="hidden-sm">Add new blog</span></a></li>

                <li><a class="submenu" href="{{route('admin.blogs.index')}}"><i class="fa fa-eye"></i><span class="hidden-sm">Show all blogs</span></a></li>

              </ul>
            </li>
            </fieldset>
                </form>
          </ul>
        </div>
            </div>


<div id="content" class="col-lg-10 col-sm-11">


  <div class="row hideInIE8">
    <div class="col-lg-12">
@yield('content')
    </div>
  </div>
</div>
    </div><!--/col-->

</div><!--/row-->
      {{--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
@include('sweet::alert')
<!-- /page container -->
    <script>
                /// loading website
    
    jQuery(window).load(function () {
        $(".loader").fadeOut(500, function () {
            $(".loading").delay(1000).fadeOut(500);
            $("body").css("overflow-y", "auto");
        });
    });
    </script>
    

@stack('scripts')
</body>
<footer>

  <div class="row">

    <div class="col-sm-5">
      &copy; 2014 creativeLabs. <a href="http://bootstrapmaster.com">Admin Templates</a> by BootstrapMaster
    </div><!--/.col-->

    <div class="col-sm-7 text-right">
      Powered by: <a href="http://bootstrapmaster.com/demo/simpliq/" alt="Bootstrap Admin Templates">SimpliQ Dashboard</a> | Based on Bootstrap 3.1.1 | Built with brix.io <a href="http://brix.io" alt="Brix.io - Interface Builder">Interface Builder</a>
    </div><!--/.col-->

  </div><!--/.row-->

</footer>

<!-- start: JavaScript-->
<!--[if !IE]>-->

<script src="{{asset('public/admin/assets/js/jquery-3.1.0.min.js')}}"></script>

<!--<![endif]-->

<!--[if IE]>

<script src="{{asset('admin/assets/js/jquery-1.11.0.min.js')}}"></script>

<![endif]-->

<!--[if !IE]>-->


<!--<![endif]-->

<!--[if IE]>
<script type="text/javascript">
  window.jQuery || document.write("<script src='{{asset('admin/assets/js/jquery-3.1.0.min.js')}}>"+"<"+"/script>");
</script>
<script type="text/javascript">
  window.jQuery || document.write("<script src='{{asset('admin/assets/js/jquery-1.11.0.min.js')}}>"+"<"+"/script>");
</script>

<![endif]-->
<script src="{{asset('public/admin/assets/js/jquery-migrate-1.4.1.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/bootstrap.min.js')}}"></script>




<!-- page scripts -->
<script src="{{asset('public/admin/assets/js/jquery-ui-1.10.3.custom.min.js')}}"></script>
<script src="{{asset('public/admin/assets/jquery-confirm/jquery.confirm.min.js')}}"></script>

<script src="{{asset('public/admin/assets/js/jquery.noty.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/jquery.raty.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/jquery.gritter.min.js')}}"></script>

<!-- theme scripts -->
<script src="{{asset('public/admin/assets/js/custom.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/core.min.js')}}"></script>

<!-- inline scripts related to this page -->
<script src="{{asset('public/admin/assets/js/pages/ui-elements.js')}}"></script>
<script src="{{asset('public/admin/assets/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/pages/table.js')}}"></script>
<script src="{{asset('public/admin/assets/sweetalert2.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/bootstrap-fileinput/js/fileinput_locale_el.js')}}"></script>
<script src="{{asset('public/admin/plugins/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{asset('public/admin/assets/summernote.js')}}"></script>


<!-- end: JavaScript-->
@include('admin/f_message')

<script type="text/javascript">

    $(document).on('click', '.destroy', function () {
        var route = $(this).data('route');
        var token = $(this).data('token');
        $.confirm({
            icon: 'glyphicon glyphicon-floppy-remove',
            animation: 'rotateX',
            closeAnimation: 'rotateXR',
            title: 'تأكيد عملية الحذف',
            autoClose: 'cancel|6000',
            text: 'هل أنت متأكد من الحذف ؟',
            confirmButtonClass: 'btn-outline',
            cancelButtonClass: 'btn-outline',
            confirmButton: 'نعم',
            cancelButton: 'لا',
            dialogClass: "modal-danger modal-dialog",
            confirm: function () {
                $.ajax({
                    url: route,
                    type: 'post',
                    data: {_method: 'delete', _token: token},
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 0) {
                            //toastr.error(data.msg)
                            swal("خطأ!", data.msg, "error")
                        } else {
                            $("#removable" + data.id).remove();
                            swal("Good!", data.msg, "success")
                            //toastr.success(data.msg)
                        }
                    }
                });
            },
        });

    });

    $(".file_upload_preview").fileinput({
        showUpload: false,
        showRemove: false,
        showCaption: false
    });
    
</script>
    <script>
                /// loading website
    
    jQuery(window).load(function () {
        $(".loader").fadeOut(500, function () {
            $(".loading").delay(1000).fadeOut(500);
            $("body").css("overflow-y", "auto");
        });
    });
    </script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture']],
            ]
        });


        $(function () {
            $('.file_upload_preview').watermark({

                path: '{{url('/images/logo.png')}}'
            });
        });

    });
</script>

    
</body>
</html>
