@extends('admin.layout')
@section('title') add new category
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new category
        </div>
        <div class="box-content">
            {!! Form::model($category,
                          ['route'=>['admin.category.update',$category->id],
                           'method'=>'PATCH',
                           'files' => true
            ])!!}
            @include('admin.categories.form')

            {!!Form::close() !!}


        </div>
    </div>

@endsection

