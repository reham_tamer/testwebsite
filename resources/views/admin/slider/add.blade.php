@extends('admin.layout')
@section('title') add new slider
@endsection

@section('header')

@endsection

@section('content')
    <!-- Vertical form options -->

    <div class="box">
        <div class="box-header">
            Add new slider
        </div>
        <div class="box-content">
            {!! Form::open([
               'route' => 'admin.slider.store',
               'method' => 'POST',
               'class' => 'form-horizontal',
               'files' => true
             ]) !!}
            @include('admin.slider.form')
            {!!Form::close() !!}

        </div>
    </div>

@endsection

